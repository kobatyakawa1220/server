## Golang references

These are references what i saw to implement this.

### Golang environment installation

- https://qiita.com/fsd-osw/items/0e569b0b828455cdef9e
- https://iwasiman.hatenablog.com/entry/20220516-go-env-on-vscode
  - Install all go extension recommendations
- https://log.include.co.jp/2021/01/11/golang_1/
- https://github.com/golangci/golangci-lint

### DDD examples

- https://github.com/Tim0401/oapi-codegen-demo
- https://github.com/gs1068/golang-ddd-sample
- https://github.com/wmetaw/go-ddd-on-echo
- https://qiita.com/ryokky59/items/6c2b35169fb6acafce15

### API implementation examples

- https://github.com/UsagiBooru/accounts-server
