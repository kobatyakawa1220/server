## Folder structure

The client requests handled by below steps:

- `main.go`
  - entry point
- `infrastructure/router.go`
- `infrastructure/middleware/session.go`
  - create http router
- interface/example.go
- interface/example_service.go
  - parse request and call usecase
- application/usecase
  - main logic
- domain/model
  - data behavior
- domain/repository
  - data access interface
- infrastructure/persistence
  - data access
