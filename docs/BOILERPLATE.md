## Boilerplate

DDD project has much files and lazy to make them step by step. So I created boilerplate to help you to create the files.

### How to use

- Make a terminal at the root of your project
- Run `go run boilerplate/main.go <model_name_in_snake_case>`
- Now you'll get below files:
  - application/usecase/<model_name>\_usecase.go
  - domain/repository/<model_name>\_repository.go
  - infrastructure/persistence/<model_name>\_persistence.go
  - domain/model/<model_name>\_domain.go

Ideally, they should support [mock](https://github.com/golang/mock) but not yet done.

### References

- [ボイラープレートを一括生成するコマンドを作る](https://www.sunapro.com/go-text-template/)
- [go generate でモックを生成して呼び出すまで](https://ybalexdp.hatenablog.com/entry/2022/09/23/170919)
- [Poe / sage](https://poe.com/sage)

### Related

- Template generator
  - https://github.com/phcollignon/Go-Template
- Task runner
  - [Go で書けるタスクランナー Mage が快適すぎて捗る](https://qiita.com/townewgokgok/items/faad9327927947646a23)
- CLI library
  - https://github.com/c-bata/go-prompt
