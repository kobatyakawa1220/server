## Git

This doc describes some notes about the git usage.

### Delete merged local branches

Below command deletes local branches which is already merged to origin.
(Windows requires `wsl` or `bash`)

```bash
git branch --merged|egrep -v '\*|develop|main'|xargs git branch -d
```

### Sync deleted(merged) remote branch info

Below command deletes local branch info which is already deleted in remote repository.
(It doesn't affect remote branches)

```bash
git remote prune origin
git remote prune upstream
```

## Source

- https://qiita.com/makoto_kw/items/c825e17e2a577bb83e19
- https://qiita.com/hajimeni/items/73d2155fc59e152630c4
