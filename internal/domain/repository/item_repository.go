package repository

import (
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
)

type ItemRepository interface {
	FindItem(query *model_item.Item, criteria map[string]interface{}, associations *[]string) (*model_item.Item, error)
	FindItems(query *model_item.Item, criteria map[string]interface{}, associations *[]string) ([]*model_item.Item, error)
	GetWeaponUpgradeAmount(itemId int64) (int32, error)
	GetCharacterUpgradeAmount(itemId int64) (int32, int32, error)
}
