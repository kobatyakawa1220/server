package repository

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableCharacterRepository interface {
	FindExpTableCharacter(query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) (*model_exp_table.ExpTableCharacter, error)
	FindExpTableCharacters(query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) ([]*model_exp_table.ExpTableCharacter, error)
	GetRequiredCoinsForUpgrade(currentLevel uint8) (uint16, error)
}
