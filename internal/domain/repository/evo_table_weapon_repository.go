package repository

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type EvoTableWeaponRepository interface {
	FindByWeaponId(weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error)
}
