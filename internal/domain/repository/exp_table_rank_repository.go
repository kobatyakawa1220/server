package repository

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableRankRepository interface {
	FindExpTableRank(query *model_exp_table.ExpTableRank, criteria map[string]interface{}) (*model_exp_table.ExpTableRank, error)
	FindExpTableRanks(query *model_exp_table.ExpTableRank, criteria map[string]interface{}) ([]*model_exp_table.ExpTableRank, error)
}
