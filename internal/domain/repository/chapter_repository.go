package repository

import (
	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
)

type ChapterRepository interface {
	GetAll() ([]*model_chapter.Chapter, error)
}
