package repository

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableWeaponRepository interface {
	FindExpTableWeapon(query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) (*model_exp_table.ExpTableWeapon, error)
	FindExpTableWeapons(query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) ([]*model_exp_table.ExpTableWeapon, error)
	GetRequiredCoinsForUpgrade(currentLevel uint8) (uint16, error)
}
