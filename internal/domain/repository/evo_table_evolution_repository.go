package repository

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type EvoTableEvolutionRepository interface {
	FindByCharacterId(characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error)
}
