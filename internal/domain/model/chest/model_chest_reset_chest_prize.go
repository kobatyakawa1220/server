package model_chest

type ResetChestPrize struct {
	// Foreign key
	ChestId int64

	CurrentStock uint16
	MaxStock     uint16

	ResetTarget  int64
	RewardType   int64
	RewardId     int64
	RewardAmount int64
}
