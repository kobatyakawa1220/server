package model_level_table

type LevelTableTownFacilityFieldItemDropProbability struct {
	LevelTableFacilityId *uint `gorm:"primaryKey"`
	// Index
	FieldItemDropProbabilityId uint32
	// 0~100 (total)
	FieldItemDropProbability uint8
}
