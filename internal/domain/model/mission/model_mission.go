package model_mission

import (
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
)

type Mission struct {
	// database internal mission info id
	MissionId int64 `gorm:"primary_key"`
	// Category at mission list screen
	Category value_mission.MissionCategory
	// Main type of mission
	MissionSegType value_mission.MissionSegType
	// Sub type of mission
	MissionFuncType value_mission.MissionFuncType
	// CharacterId for MissionSegTypeCharacterRelationship / QuestId for MissionSegTypeQuestClear (default 0)
	SubCode uint64
	// Mission name ({0} string takes the rate max value)
	TargetMessage string
	// Current progress Ex. 1 of 1/3
	Rate uint32
	// Max progress Ex. 3 of 1/3
	RateMax uint32
	// Reward of mission clear (sometimes this field can be empty)
	Reward []MissionReward
	// Tutorial mission's screen transit Default 0
	TransitParam uint8
	// Tutorial mission's screen transit Default 0
	TransitScene uint8
	// Mission re-order for list screen (higher value goes down)
	UiPriority int64
	// Custom field to handle mission system
	// This field will used as database key to find next mission
	MissionInsertType value_mission.MissionInsertType
}
