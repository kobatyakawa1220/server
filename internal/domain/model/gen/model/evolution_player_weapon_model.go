package model

type EvolutionPlayerWeaponResponse struct {
	Gold int64

	ItemSummary []ItemSummaryArrayObject

	ManagedWeapon EvolutionPlayerWeaponResponseManagedWeapon
}
