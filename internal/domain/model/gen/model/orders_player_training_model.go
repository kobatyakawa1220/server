package model

type OrdersPlayerTrainingResponse struct {
	Player BasePlayer

	SlotInfo []SlotInfoArrayObject

	TrainingInfo []TrainingInfoArrayObject
}
