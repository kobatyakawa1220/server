package model

type TrainingRewardItemsArrayObject struct {
	ItemAmount int64

	ItemId int64

	RareDisp int64
}
