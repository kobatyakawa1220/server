package model

type InformationsArrayObject struct {
	DispEndAt string

	DispStartAt string

	EndAt string

	Id int64

	ImgId string

	IsFeatured int64

	Platform int64

	Sort int64

	StartAt string

	Url string
}
