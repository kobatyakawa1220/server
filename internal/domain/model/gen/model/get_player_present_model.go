package model

type GetPlayerPresentResponse struct {
	ArousalResults []interface{}

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedMasterOrbs []ManagedMasterOrbsArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	ManagedRoomObjects []ManagedRoomObjectsArrayObject

	ManagedTownFacilities []ManagedTownFacilitiesArrayObject

	ManagedWeapons []ManagedWeaponsArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject

	Offers []interface{}

	Player BasePlayer

	Received []ReceivedArrayObject
}
