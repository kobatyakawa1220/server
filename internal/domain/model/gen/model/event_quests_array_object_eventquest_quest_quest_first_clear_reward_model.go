package model

type EventQuestsArrayObjectEventquestQuestQuestFirstClearReward struct {
	Amount1 int64

	Amount2 int64

	Amount3 int64

	Gem int64

	Gold int64

	ItemId1 int64

	ItemId2 int64

	ItemId3 int64

	RoomObjectAmount int64

	RoomObjectId int64

	WeaponAmount int64

	WeaponId int64
}
