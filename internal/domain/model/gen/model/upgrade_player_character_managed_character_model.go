package model

type UpgradePlayerCharacterResponseManagedCharacter struct {
	ArousalLevel int64

	CharacterId int64

	DuplicatedCount int64

	Exp int64

	Level int64

	LevelBreak int64

	LevelLimit int64

	ManagedCharacterId int64

	PlayerId int64

	Shown int64

	SkillExp1 int64

	SkillExp2 int64

	SkillExp3 int64

	SkillLevel1 int64

	SkillLevel2 int64

	SkillLevel3 int64

	SkillLevelLimit1 int64

	SkillLevelLimit2 int64

	SkillLevelLimit3 int64

	ViewCharacterId int64
}
