package model

type EventBanner struct {
	BannerName string `json:"bannerName,omitempty"`

	BgName string `json:"bgName,omitempty"`

	ExpiredAt *int64 `json:"expiredAt,omitempty"`

	ExpiredDay int64 `json:"expiredDay,omitempty"`

	ExtendDay int64 `json:"extendDay,omitempty"`

	HelpBannerName string `json:"helpBannerName,omitempty"`

	Id int64 `json:"id,omitempty"`

	Name string `json:"name,omitempty"`

	Notice *string `json:"notice,omitempty"`

	RemindType int64 `json:"remindType,omitempty"`

	Rewards []RewardsInner `json:"rewards,omitempty"`

	Summary *string `json:"summary,omitempty"`
}
