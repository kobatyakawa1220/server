package model

type GetAllPlayerFavoriteMemberResponse struct {
	ManagedFavoriteMembers []ManagedFavoriteMembersArrayObject
}
