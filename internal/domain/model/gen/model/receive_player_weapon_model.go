package model

type ReceivePlayerWeaponResponse struct {
	ManagedWeapons []ManagedWeaponsArrayObject

	WeaponLimit int64
}
