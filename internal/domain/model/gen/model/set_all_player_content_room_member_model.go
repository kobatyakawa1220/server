package model

type SetAllPlayerContentRoomMemberResponse struct {
	ManagedCharacterIds []int64
}
