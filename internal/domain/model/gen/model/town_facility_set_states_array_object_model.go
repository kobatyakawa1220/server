package model

type TownFacilitySetStatesArrayObject struct {
	ActionNo int64

	BuildPointIndex int64

	BuildTime int64

	ManagedTownFacilityId int64

	OpenState int64
}
