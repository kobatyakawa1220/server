package model

type DrawPlayerGachaResponse struct {
	AfterDrawPoint int64

	ArousalResults []ArousalResultsArrayObject

	BeforeDrawPoint int64

	GachaBonuses []GachaBonusesArrayObject

	GachaResults []GachaResultsArrayObject

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject

	Offers []interface{}

	Player BasePlayer
}
