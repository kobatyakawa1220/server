package model

type OrderPlayerOfferResponse struct {
	ItemSummary []ItemSummaryArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject

	// Offers []OffersArrayObject
}
