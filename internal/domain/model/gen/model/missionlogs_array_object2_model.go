package model

type MissionlogsArrayObject2 struct {
	Category int64

	LimitTime string

	ManagedMissionId int64

	MissionFuncType int64

	MissionId int64

	MissionSegType int64

	Rate int64

	RateMax int64

	Reward *string

	State int64

	SubCode       int64
	TargetMessage *string

	TransitParam int64
	TransitScene int64
	UiPriority   int64
}
