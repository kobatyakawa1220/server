package model

type ScheduleTableStruct struct {
	MDayListUp  []ScheduleTableStructMDayListUpInner
	MPlayTime   int64
	MVersion    int64
	MDropItemId int64
	MDropMagKey float32
}
