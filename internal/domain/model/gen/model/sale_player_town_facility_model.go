package model

type SalePlayerTownFacilityResponse struct {
	ManagedTownFacilities []ManagedTownFacilitiesArrayObject

	Player BasePlayer
}
