package model

type FieldPartyMembersArrayObject struct {
	ArousalLevel int64

	Character *string

	CharacterId int64

	Flag int64

	LiveIdx int64

	ManagedCharacterId int64

	ManagedFacilityId int64

	ManagedPartyMemberId int64

	PartyDropPresents *string

	RoomId int64

	ScheduleId int64

	ScheduleTable string

	ScheduleTag int64

	TouchItemResultNo int64
}
