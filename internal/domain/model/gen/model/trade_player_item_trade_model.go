package model

type TradePlayerItemTradeResponse struct {
	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	ManagedWeapons []ManagedWeaponsArrayObject

	MonthlyTradeCount int64

	Player BasePlayer

	TotalTradeCount int64
}
