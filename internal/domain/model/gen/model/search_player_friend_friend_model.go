package model

type SearchPlayerFriendResponseFriend struct {
	Comment string

	CurrentAchievementId int64

	Direction int64

	FirstFavoriteMember SearchPlayerFriendResponseFriendFirstFavoriteMember

	LastLoginAt string

	Level int64

	ManagedFriendId int64

	MyCode string

	Name string

	NamedTypes *string

	PlayerId int64

	State int64

	SupportCharacters []SupportCharactersArrayObject

	SupportLimit int64

	SupportName string

	TotalExp int64
}
