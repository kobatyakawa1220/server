package model

type ManagedBattlePartiesArrayObject struct {
	CostLimit int64

	ManagedBattlePartyId int64

	ManagedCharacterId1 int64

	ManagedCharacterId2 int64

	ManagedCharacterId3 int64

	ManagedCharacterId4 int64

	ManagedCharacterId5 int64

	ManagedCharacterIds []int64

	ManagedWeaponId1 int64

	ManagedWeaponId2 int64

	ManagedWeaponId3 int64

	ManagedWeaponId4 int64

	ManagedWeaponId5 int64

	ManagedWeaponIds []int64

	MasterOrbId int64

	Name string
}
