package model

type GetAllPlayerStoreResponse struct {
	AvailableBalance int64

	ExchangeShops []ExchangeShopsArrayObject

	MonthlyAmount int64

	Products []ProductsArrayObject
}
