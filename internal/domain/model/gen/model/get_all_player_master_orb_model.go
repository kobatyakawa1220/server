package model

type GetAllPlayerMasterOrbResponse struct {
	ManagedMasterOrbs []ManagedMasterOrbsArrayObject
}
