package model_user

import (
	"time"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestLogCharacterEquipment struct {
	QuestLogCharacterEquipmentId uint `gorm:"primarykey"`
	// foreignKey
	QuestLogCharacterId uint
	EquipmentItemId     int64
}

type QuestLogCharacter struct {
	QuestLogCharacterId uint `gorm:"primarykey"`
	// foreignKey
	QuestLogId       int
	CharacterId      value_character.CharacterId
	Index            uint8
	Level            uint8
	ArousalLevel     uint8
	SkillLevel1      uint8
	SkillLevel2      uint8
	SkillLevel3      uint8
	WeaponId         int64
	WeaponLevel      int8
	WeaponSkillLevel int8
	Equipments       []QuestLogCharacterEquipment
}

type QuestLog struct {
	QuestLogId int `gorm:"primarykey"`
	// foreignKey
	UserId uint
	User   User `gorm:"PRELOAD:false;foreignKey:UserId"`
	// foreignKey
	QuestId     uint
	Quest       model_quest.Quest `gorm:"PRELOAD:false;foreignKey:QuestId"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	ClearRank   value_quest.ClearRank
	IsRead      value.BoolLikeUInt8
	CurrentWave uint8
	TotalWave   uint8
	QuestData   string
	WaveData    *string
	DropData    *string
	Characters  []QuestLogCharacter
	Support     *QuestLogCharacter
}
