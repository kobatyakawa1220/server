package model_user

import (
	"time"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
)

type UserPresent struct {
	ManagedPresentId uint `gorm:"primary_key"`
	// Foreign Key
	UserId uint
	User   User `gorm:"PRELOAD:false"`
	// foreignKey
	PresentId  uint `gorm:"ForeignKey:PresentId"`
	Present    model_present.Present
	CreatedAt  time.Time
	ReceivedAt *time.Time
	DeadlineAt time.Time
	// Since tutorial items needs template but making all item combinations as template takes too much space.
	Type value_present.PresentType
	// ItemId or CharacterId
	ObjectId int64
	Amount   int64
}
