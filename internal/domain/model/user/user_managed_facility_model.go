package model_user

import (
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
)

type ManagedTownFacility struct {
	ManagedTownFacilityId uint `gorm:"primary_key"`
	// Foreign key
	UserId     uint
	FacilityId uint32
	// Placement at town (0~4: main menu / other)
	BuildPointIndex value_town_facility.BuildPointIndex
	// Facility level (1~10)
	Level uint8
	// 0: not opened / 1: opened
	OpenState value_town_facility.TownFacilityOpenState
	// Unix time
	ActionTime uint64
	BuildTime  uint64
}
