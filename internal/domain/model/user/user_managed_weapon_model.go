package model_user

import (
	"time"

	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedWeapon struct {
	ManagedWeaponId value_user.ManagedWeaponId `gorm:"primarykey"`
	// Foreign key
	UserId uint
	// WeaponId (character specific weapon has same Id as characterId, so 16 is not enough)
	WeaponId   value_weapon.WeaponId
	Level      uint8               `json:"level"`
	Exp        value_exp.WeaponExp `json:"exp"`
	SkillLevel uint8               `json:"skillLevel"`
	SkillExp   uint32              `json:"skillExp"`
	// Unknown static value (default: 1)
	State uint8 `json:"state"`
	// Unknown static value (default: 0001-01-01T00:00:00)
	SoldAt *time.Time
}

func (m *ManagedWeapon) UpdateWeaponLevel(level uint8) {
	newLevel := calc.Max(m.Level, level)
	// FIXME: Move this constant value to something other
	m.Level = calc.Min(newLevel, 100)
}

func (w *ManagedWeapon) AddWeaponLevel(level uint8) {
	w.Level += level
}

func (w *ManagedWeapon) AddWeaponExp(exp value_exp.WeaponExp) {
	w.Exp += exp
}

func (w *ManagedWeapon) UpdateWeaponSkillLevel(level uint8) {
	w.SkillLevel = calc.Max(w.SkillLevel, level)
}

func (w *ManagedWeapon) AddWeaponSkillExp(exp uint32) {
	w.SkillExp += exp
}
