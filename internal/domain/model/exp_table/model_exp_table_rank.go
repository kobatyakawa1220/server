package model_exp_table

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type ExpTableRank struct {
	Rank            uint `gorm:"primaryKey"`
	NextExp         uint64
	TotalExp        uint32
	Stamina         uint16
	FriendLimit     uint16
	SupportLimit    uint8
	BattlePartyCost uint8
	WeaponLimit     uint8
	TrainingSlotNum uint8
	StoreReview     value.BoolLikeUInt8
}
