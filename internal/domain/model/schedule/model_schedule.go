package model_schedule

type ScheduleDropItem struct {
	DropItemId uint32   `json:"drop_item_id"`
	Period     [2]uint8 `json:"period"`
}

type Schedule struct {
	ScheduleTable string             `json:"schedule_table"`
	DropItems     []ScheduleDropItem `json:"drop_items"`
}
