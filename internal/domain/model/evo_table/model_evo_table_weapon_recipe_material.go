package model_evo_table

type EvoTableWeaponRecipeMaterial struct {
	Id       uint `gorm:"primaryKey"`
	RecipeId uint
	ItemId   uint32
	Amount   uint32
}
