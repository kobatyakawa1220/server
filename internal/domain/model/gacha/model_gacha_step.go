package model_gacha

type GachaStep struct {
	GachaStepId     uint `gorm:"primary_key"`
	Step            uint8
	GachaBonusItems []GachaStepBonusItem
	// Foreign key
	GachaId uint
}
