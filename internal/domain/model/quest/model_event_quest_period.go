package model_quest

import "time"

type EventQuestPeriodGroupQuestCondEventQuestId struct {
	EventQuestPeriodGroupQuestCondEventQuestId uint `gorm:"primary_key"`
	// Foreign key
	EventQuestPeriodGroupQuestId uint
	CondEventQuestId             uint
}

type EventQuestPeriodGroupQuest struct {
	EventQuestPeriodGroupQuestId uint `gorm:"primary_key"`
	// Foreign key
	EventQuestPeriodGroupId uint
	StartAt                 time.Time
	EndAt                   time.Time
	CondEventQuestIds       []EventQuestPeriodGroupQuestCondEventQuestId
	QuestId                 int64
	EventQuestId            int64
	Unlocked                bool
}

type EventQuestPeriodGroup struct {
	EventQuestPeriodGroupId uint `gorm:"primary_key"`
	// Foreign key
	EventQuestPeriodId          uint
	StartAt                     time.Time
	EndAt                       time.Time
	EventQuestPeriodGroupQuests []EventQuestPeriodGroupQuest
	GroupId                     int64
	Unlocked                    bool
}

type EventQuestPeriod struct {
	EventQuestPeriodId     uint `gorm:"primary_key"`
	StartAt                time.Time
	EndAt                  time.Time
	LossTimeEndAt          *time.Time
	EventType              int64
	EventQuestPeriodGroups []EventQuestPeriodGroup
}
