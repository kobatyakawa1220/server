package model_quest

import value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"

type OfferQuest struct {
	OfferQuestId uint `gorm:"primaryKey"`
	OfferId      uint
	// Foreign key
	QuestId   uint
	Quest     Quest
	Category  value_quest.QuestCategoryType
	TitleType int64
}
