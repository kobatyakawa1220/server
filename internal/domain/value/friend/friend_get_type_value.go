package value_friend

import "errors"

type FriendGetType uint8

const (
	FriendGetTypeFriend FriendGetType = iota + 1
	FriendGetTypeGuest
	FriendGetTypeBoth
)

var ErrInvalidFriendGetType = errors.New("invalid friend get type")

func NewFriendGetType(v uint8) (FriendGetType, error) {
	if v > uint8(FriendGetTypeBoth) {
		return FriendGetTypeFriend, ErrInvalidFriendGetType
	}
	return FriendGetType(v), nil
}
