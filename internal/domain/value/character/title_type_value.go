package value_character

import "errors"

type TitleType int8

const (
	TitleTypeNone TitleType = iota - 1
	TitleTypeHidamari
	TitleTypeYuyushiki
	TitleTypeGakkou
	TitleTypeAchannel
	TitleTypeKinmosa
	TitleTypeNewGame
	TitleTypeStella
	TitleTypeUrara
	TitleTypeKillme
	TitleTypeSakura
	TitleTypeBlends
	TitleTypeSlowStart
	TitleTypeKeion
	TitleTypeYurukyan
	TitleTypeKomiga
	TitleTypeGA
	TitleTypeYumekui
	TitleTypeHanayamata
	TitleTypeAnhapi
	TitleTypeHarukana
	TitleTypeGochiusa
	TitleTypeAnima
	TitleTypeKirara
	TitleTypeSansha
	TitleTypeHitugi
	TitleTypeMatikado
	TitleTypeHarumination
	TitleTypeKoufuku
	TitleTypeKoiasu
	TitleTypeTamayomi
	TitleTypeAcchikocchi
	TitleTypeOchifuru
	TitleTypePawasuma
	TitleTypeKoharu
	TitleTypeMangaTime
	TitleTypeSlowLoop
	TitleTypeRpg
	TitleTypeBotti
)

var ErrInvalidTitleType = errors.New("invalid TitleType")

func NewTitleType(v int8) (TitleType, error) {
	if v > int8(TitleTypeBotti) || v < int8(TitleTypeNone) {
		return TitleTypeHidamari, ErrInvalidTitleType
	}
	return TitleType(v), nil
}
