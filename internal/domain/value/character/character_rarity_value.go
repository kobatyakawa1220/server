package value_character

import "errors"

type CharacterRarity = uint8

const (
	CharacterRarityStar3 CharacterRarity = iota + 3
	CharacterRarityStar4
	CharacterRarityStar5
)

var ErrInvalidCharacterRarity = errors.New("invalid character rarity")

func NewCharacterRarity(value uint8) (CharacterRarity, error) {
	if value < CharacterRarityStar3 {
		return 0, ErrInvalidCharacterRarity
	}
	if value > CharacterRarityStar5 {
		return 0, ErrInvalidCharacterRarity
	}
	return CharacterRarity(value), nil
}
