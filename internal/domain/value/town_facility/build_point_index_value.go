package value_town_facility

import "errors"

type BuildPointIndex int32

const (
	BuildPointIndexNotPlaced           BuildPointIndex = -1
	BuildPointIndexMainMenuCenter      BuildPointIndex = 0x100004B0
	BuildPointIndexMainMenuBottomRight BuildPointIndex = 0x100004B2
	BuildPointIndexMainMenuUpperLeft   BuildPointIndex = 0x100004B3
	BuildPointIndexMainMenuBottomLeft  BuildPointIndex = 0x100004B4
	BuildPointIndexMainMenuUpperRight  BuildPointIndex = 0
	BuildPointIndexTownMessageBoard    BuildPointIndex = 0x100004B5

	BuildPointIndexGeneral1 BuildPointIndex = 0x00000001
	BuildPointIndexGeneral2 BuildPointIndex = 0x00000002
	BuildPointIndexGeneral3 BuildPointIndex = 0x00000003
	BuildPointIndexGeneral4 BuildPointIndex = 0x00000004
	BuildPointIndexGeneral5 BuildPointIndex = 0x00000005
	BuildPointIndexGeneral6 BuildPointIndex = 0x00000006

	// TODO: Find which location of town maps to these indexes
	BuildPointIndexUnknown1 BuildPointIndex = 0x20000001
	BuildPointIndexUnknown2 BuildPointIndex = 0x20000002
	BuildPointIndexUnknown3 BuildPointIndex = 0x20000003
	BuildPointIndexUnknown4 BuildPointIndex = 0x20000004
	BuildPointIndexUnknown5 BuildPointIndex = 0x20000005
	BuildPointIndexUnknown6 BuildPointIndex = 0x20000006

	BuildPointIndexUnknown7  BuildPointIndex = 0x40000001
	BuildPointIndexUnknown8  BuildPointIndex = 0x40000002
	BuildPointIndexUnknown9  BuildPointIndex = 0x40000003
	BuildPointIndexUnknown10 BuildPointIndex = 0x40000004
	BuildPointIndexUnknown11 BuildPointIndex = 0x40000005
	BuildPointIndexUnknown12 BuildPointIndex = 0x40000006

	BuildPointIndexUnknown13 BuildPointIndex = 0x40010001
	BuildPointIndexUnknown14 BuildPointIndex = 0x40010002
	BuildPointIndexUnknown15 BuildPointIndex = 0x40010003
	BuildPointIndexUnknown16 BuildPointIndex = 0x40010004
	BuildPointIndexUnknown17 BuildPointIndex = 0x40010005
	BuildPointIndexUnknown18 BuildPointIndex = 0x40010006

	BuildPointIndexUnknown19 BuildPointIndex = 0x40020001
	BuildPointIndexUnknown20 BuildPointIndex = 0x40020002
	BuildPointIndexUnknown21 BuildPointIndex = 0x40020003
	BuildPointIndexUnknown22 BuildPointIndex = 0x40020004
	BuildPointIndexUnknown23 BuildPointIndex = 0x40020005
	BuildPointIndexUnknown24 BuildPointIndex = 0x40020006

	BuildPointIndexUnknown25 BuildPointIndex = 0x40030001
	BuildPointIndexUnknown26 BuildPointIndex = 0x40030002
	BuildPointIndexUnknown27 BuildPointIndex = 0x40030003
	BuildPointIndexUnknown28 BuildPointIndex = 0x40030004
	BuildPointIndexUnknown29 BuildPointIndex = 0x40030005
	BuildPointIndexUnknown30 BuildPointIndex = 0x40030006
)

var ErrInvalidBuildPointIndex = errors.New("invalid build point index")

func NewBuildPointIndex(value int32) (BuildPointIndex, error) {
	if value < int32(BuildPointIndexNotPlaced) || value > int32(BuildPointIndexUnknown30) {
		return BuildPointIndex(-1), ErrInvalidBuildPointIndex
	}
	return BuildPointIndex(value), nil
}
