package value_user

import (
	"golang.org/x/exp/constraints"
)

type ManagedSupportId uint

func (id *ManagedSupportId) ToValue() uint {
	return uint(*id)
}

func NewManagedSupportId[T constraints.Integer](id T) ManagedSupportId {
	return ManagedSupportId(uint(id))
}
