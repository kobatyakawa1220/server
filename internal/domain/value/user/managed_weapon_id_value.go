package value_user

import (
	"golang.org/x/exp/constraints"
)

type ManagedWeaponId int64

func (id *ManagedWeaponId) ToValue() int64 {
	return int64(*id)
}

func NewManagedWeaponId[T constraints.Integer](id T) ManagedWeaponId {
	return ManagedWeaponId(int64(id))
}
