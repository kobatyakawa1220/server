package value_offer

import "errors"

type OfferState uint8

const (
	OfferStateNone OfferState = iota
	OfferStateNotStarted
	// Not-enough crea
	OfferStateProgress
	// Enough crea and need quest clear
	OfferStateProgressQuestHappen
	OfferStateDone
)

var ErrInvalidOfferState = errors.New("invalid OfferState")

func NewOfferState(v uint8) (OfferState, error) {
	if v > uint8(OfferStateDone) {
		return OfferStateNone, ErrInvalidOfferState
	}
	return OfferState(v), nil
}
