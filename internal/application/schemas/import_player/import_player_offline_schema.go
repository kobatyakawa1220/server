package schema_import_player

import (
	"time"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type ImportPlayerOfflineSchema struct {
	Name                 string
	Comment              string
	MyCode               string
	AchievementID        int64
	Level                uint8
	LevelExp             int64
	TotalExp             int64
	Gold                 uint64
	UnlimitedGem         uint32
	LimitedGem           uint32
	PartyCost            uint16
	Stamina              uint32
	StaminaMax           uint32
	RecastTime           uint16
	RecastTimeMax        uint16
	Kirara               uint32
	KiraraLimit          uint32
	WeaponLimit          uint16
	WeaponLimitCount     uint16
	FacilityLimit        uint16
	FacilityLimitCount   uint16
	RoomObjectLimit      uint16
	RoomObjectLimitCount uint16
	SupportLimit         uint8
	LoginCount           uint32
	LastLoginAt          time.Time
	LoginDays            uint16
	ContinuousDays       uint16
	ManagedCharacters    []model_user.ManagedCharacter
	ManagedNamedTypes    []model_user.ManagedNamedType
	AdvIds               []model_user.ClearedAdvId
}
