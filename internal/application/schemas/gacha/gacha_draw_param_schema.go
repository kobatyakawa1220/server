package schema_gacha

import value_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/value/gacha"

// Gacha draw request param
type GachaDrawParamSchema struct {
	DrawType            value_gacha.GachaDrawType
	IsChanceUp          bool
	Is10Roll            bool
	SelectedCharacterId uint64
}
