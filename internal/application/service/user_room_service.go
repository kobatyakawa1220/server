package service

import (
	schema_room "gitlab.com/kirafan/sparkle/server/internal/application/schemas/room"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
)

type UserRoomService interface {
	SetPlayerRoom(
		internalUserId uint,
		params schema_room.SetPlayerRoomSchema,
	) error
	GetAllPlayerRoom(
		internalUserId uint,
		floorId value_room.FloorId,
	) ([]model_user.ManagedRoom, error)
}

type userRoomService struct {
	uu usecase.UserUsecase
}

func NewUserRoomService(
	uu usecase.UserUsecase,
) UserRoomService {
	return &userRoomService{uu}
}

func (s *userRoomService) SetPlayerRoom(
	internalUserId uint,
	params schema_room.SetPlayerRoomSchema,
) error {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true})
	if err != nil {
		return err
	}

	// NOTE: Group id and floorId are ignored since they can not change with this API
	user.UpdateRoom(params.ManagedRoomId, params.ArrangeData)

	// Update user
	if _, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedRooms: true}); err != nil {
		return err
	}
	return nil
}

func (s *userRoomService) GetAllPlayerRoom(
	internalUserId uint,
	floorId value_room.FloorId,
) ([]model_user.ManagedRoom, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedRooms: true})
	if err != nil {
		return nil, err
	}
	specifiedRoom, err := user.GetManagedRoomByFloorId(floorId)
	if err != nil {
		return nil, err
	}
	return []model_user.ManagedRoom{*specifiedRoom}, nil
}
