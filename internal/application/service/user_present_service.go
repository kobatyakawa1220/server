package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type UserPresentService interface {
	ReceivePresents(userId uint, managedPresentIds []int64, stepCode int64) (*model_user.User, []model_user.UserPresent, error)
}

type userPresentService struct {
	uu usecase.UserUsecase
	cu usecase.CharacterUsecase
	nu usecase.NamedTypeUsecase
}

func NewUserPresentService(
	uu usecase.UserUsecase,
	cu usecase.CharacterUsecase,
	nu usecase.NamedTypeUsecase,
) UserPresentService {
	return &userPresentService{uu: uu, cu: cu, nu: nu}
}

var ErrPresentServiceUserNotFound = errors.New("user not found")
var ErrPresentServicePresentOutOfPeriod = errors.New("specified presentIds includes out of period present")
var ErrPresentServiceInvalidStepCode = errors.New("invalid stepCode")
var ErrPresentServiceServerExplode = errors.New("server exploded")

func (s *userPresentService) ReceivePresents(userId uint, managedPresentIds []int64, stepCode int64) (*model_user.User, []model_user.UserPresent, error) {
	// Get presents that belong to the player
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{Presents: true})
	if err != nil || user.Presents == nil {
		return nil, nil, ErrPresentServiceUserNotFound
	}
	// Validate present ids
	presentIds := []int64{}
	for _, present := range user.Presents {
		presentIds = append(presentIds, int64(present.ManagedPresentId))
	}
	for _, managedPresentId := range managedPresentIds {
		if !calc.Contains(presentIds, managedPresentId) {
			return nil, nil, ErrPresentServicePresentOutOfPeriod
		}
	}
	// Get target user
	user, err = s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{
		Presents:           true,
		ItemSummary:        true,
		ManagedCharacters:  true,
		ManagedNamedTypes:  true,
		ManagedMasterOrbs:  true,
		ManagedRoomObjects: true,
		ManagedFacilities:  true,
		ManagedWeapons:     true,
		OfferTitleTypes:    true,
	})
	if err != nil || user.Presents == nil {
		return nil, nil, ErrPresentServiceUserNotFound
	}
	// Receive present
	received := []model_user.UserPresent{}
	for _, p := range user.Presents {
		var present *model_user.UserPresent
		present, err = user.ReceivePresent(p.ManagedPresentId)
		if err != nil {
			return nil, nil, ErrPresentServiceServerExplode
		}
		received = append(received, *present)
	}

	for _, received := range received {
		// Add NamedType
		if received.Type == value_present.PresentTypeCharacter {
			characterId, err := value_character.NewCharacterId(uint32(received.ObjectId))
			if err != nil {
				return nil, nil, ErrPresentServiceServerExplode
			}
			character, err := s.cu.GetCharacterById(characterId)
			if err != nil {
				return nil, nil, ErrPresentServiceServerExplode
			}
			namedType, err := s.nu.GetNamedTypeById(uint(character.NamedType))
			if err != nil {
				return nil, nil, ErrPresentServiceServerExplode
			}
			// NOTE: Ignore the duplicated error
			user.AddNamedType(uint16(namedType.NamedType), namedType.TitleType)
		}
	}

	// Update stepCode
	newStepCode, err := value_user.NewStepCode(int8(stepCode))
	if err != nil {
		return nil, nil, ErrPresentServiceInvalidStepCode
	}
	user.UpdateStepCode(newStepCode)

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{Presents: true, ItemSummary: true, ManagedCharacters: true, ManagedNamedTypes: true})
	if err != nil {
		return nil, nil, ErrPresentServiceServerExplode
	}

	return user, received, nil
}
