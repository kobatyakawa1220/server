package service

import (
	"errors"

	schema_character "gitlab.com/kirafan/sparkle/server/internal/application/schemas/character"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/upgrade"
)

type CharacterService interface {
	// Consume item and add exps to character
	UpgradeCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, items []schema_character.ConsumeItem) (schema_character.UpgradeCharacterResponseSchema, error)
	// Consume item and evolute the character
	EvoluteCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, recipeId int64) (schema_character.EvoluteCharacterResponseSchema, error)
	// Consume item and increase level limit
	LimitBreakCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, itemIds []int32) (schema_character.LimitBreakCharacterResponseSchema, error)
	// Set character's view evolute or not
	SetViewCharacter(internalUserId uint, managedCharacterIds []value_user.ManagedCharacterId, evolved []bool) ([]model_user.ManagedCharacter, error)
	// Reset all character's view to default
	ResetViewAllCharacter(internalUserId uint) ([]model_user.ManagedCharacter, error)
	// Remove character's NEW notify from screen
	SetShownCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, shown bool) error
	// Set player's support party
	SetSupportCharacters(internalUserId uint, req schema_character.SetSupportCharacterRequestSchema) error
	// Set player's support party name
	SetSupportCharactersName(internalUserId uint, managedSupportId value_user.ManagedSupportId, name string) error
}

type characterService struct {
	uu  usecase.UserUsecase
	iu  usecase.ItemUsecase
	cu  usecase.CharacterUsecase
	eu  usecase.ExpTableCharacterUsecase
	elu usecase.EvoTableLimitBreakUsecase
	eeu usecase.EvoTableEvolutionUsecase
	ch  upgrade.UpgradeCharacterHandler
}

func NewCharacterService(
	uu usecase.UserUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	eu usecase.ExpTableCharacterUsecase,
	eeu usecase.EvoTableEvolutionUsecase,
	elu usecase.EvoTableLimitBreakUsecase,
) CharacterService {
	ch := upgrade.NewUpgradeCharacterHandler(nil)
	return &characterService{uu, iu, cu, eu, elu, eeu, ch}
}

func (s *characterService) UpgradeCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, items []schema_character.ConsumeItem) (schema_character.UpgradeCharacterResponseSchema, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ItemSummary:       true,
	})
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	// Validate character exists
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	// Consume items and calculate required coins
	baseRequiredGolds, err := s.eu.GetRequiredCoinsForUpgrade(managedCharacter.Level)
	requiredGolds := uint64(0)
	for _, item := range items {
		if consumed := user.ConsumeItem(uint32(item.ItemId), uint32(item.Count)); !consumed {
			return schema_character.UpgradeCharacterResponseSchema{}, err
		}
		requiredGolds += uint64(item.Count) * baseRequiredGolds.ToValue()
	}
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	character, err := s.cu.GetCharacterById(managedCharacter.CharacterId)
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}
	increaseExps := uint64(0)
	for _, item := range items {
		class, amount, err := s.iu.GetCharacterUpgradeAmount(int64(item.ItemId))
		if err != nil {
			return schema_character.UpgradeCharacterResponseSchema{}, err
		}
		// Add class bonus
		if character.Class == uint8(class) && class != value_character.ClassTypeNone {
			// FIXME: Move this constant to something else
			// Source: https://kirarafantasia.boom-app.wiki/entry/266
			amount += value_exp.CharacterExp(float32(amount) * 0.2)
		}
		increaseExps += uint64(amount) * uint64(item.Count)
	}

	// Roll bonus
	// Source: https://kirarafantasia.miraheze.org/wiki/Upgrade
	bonus := s.ch.Roll()
	switch bonus {
	case upgrade.CharacterUpgradeResultPerfect:
		// FIXME: Move this constant to something else
		increaseExps += uint64(float32(increaseExps))
	case upgrade.CharacterUpgradeResultGreat:
		// FIXME: Move this constant to something else
		increaseExps += uint64(float32(increaseExps) * 0.5)
	}

	// Increase character exp
	managedCharacter.AddExp(increaseExps)
	// Recalculate character level
	nextCharacterLevel, err := s.eu.GetNextExpTableCharacter(managedCharacter.Exp)
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}
	managedCharacter.UpdateLevel(uint8(nextCharacterLevel.Level))

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
	})
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	resp := schema_character.UpgradeCharacterResponseSchema{
		ManagedCharacter: *managedCharacter,
		ItemSummary:      user.ItemSummary,
		Gold:             user.Gold,
		UpgradeResult:    bonus,
	}
	return resp, nil
}

func (s *characterService) EvoluteCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, recipeId int64) (schema_character.EvoluteCharacterResponseSchema, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ItemSummary:       true,
	})
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}
	// Get managed character
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}
	characterId := managedCharacter.CharacterId
	if characterId.IsEvolved() {
		return schema_character.EvoluteCharacterResponseSchema{}, errors.New("character already evolved")
	}
	// Get evolution recipe
	evolutionRecipe, err := s.eeu.GetEvolutionRecipe(characterId)
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}

	// Consume golds
	requiredGolds := uint64(evolutionRecipe.RequiredCoin)
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}
	// Consume items
	for _, itemInfo := range evolutionRecipe.RequiredItems {
		if consumed := user.ConsumeItem(uint32(itemInfo.ItemId), uint32(itemInfo.Amount)); !consumed {
			return schema_character.EvoluteCharacterResponseSchema{}, err
		}
	}

	// Updates levelLimit and display only
	// Source: https://kirarafantasia.miraheze.org/wiki/Overview_of_how_to_max_a_character%27s_level
	managedCharacter.LevelLimit += 10
	newCharacterId := characterId.SetEvoluteState(true)
	managedCharacter.CharacterId = newCharacterId
	managedCharacter.ViewCharacterId = newCharacterId
	// After evolved, the character's duplicated count will be reset
	managedCharacter.LevelBreak = 0

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
	})
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}

	resp := schema_character.EvoluteCharacterResponseSchema{
		ManagedCharacter: *managedCharacter,
		ItemSummary:      user.ItemSummary,
		Gold:             user.Gold,
	}
	return resp, nil
}

func (s *characterService) LimitBreakCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, itemIds []int32) (schema_character.LimitBreakCharacterResponseSchema, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ItemSummary:       true,
	})
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	// Validate character exists
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}
	// Validate character can limit break
	if !managedCharacter.IsLimitBreakable(uint8(len(itemIds))) {
		return schema_character.LimitBreakCharacterResponseSchema{}, errors.New("can not limit break specified count for this character")
	}

	// Get character's limit break item recipe
	character, err := s.cu.GetCharacterById(managedCharacter.CharacterId)
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}
	recipeId := uint(character.LimitBreakRecipeID)

	// Get limitBreak recipe
	recipe, err := s.elu.GetLimitBreakRecipe(recipeId)
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	// Consume golds
	baseRequiredGolds := value_user.NewGold(uint64(recipe.RequiredCoinPerItem))
	requiredGolds := value_user.NewGold(0)
	for range itemIds {
		requiredGolds += baseRequiredGolds
	}
	if consumed := user.ConsumeGold(requiredGolds.ToValue()); !consumed {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	// Consume items
	titleType := managedCharacter.CharacterId.GetContent().ToTitleType()
	// FIXME: Move this constant to somewhere else
	targetTitleItemId := uint32(5000) + uint32(titleType)
	// TODO: Read Item Amount fields from recipe. It ignored since it was not used in game.
	for _, itemId := range itemIds {
		switch itemId {
		case recipe.AllClassItemId:
			fallthrough
		case recipe.ClassItemId:
			if consumed := user.ConsumeItem(uint32(itemId), 1); !consumed {
				return schema_character.LimitBreakCharacterResponseSchema{}, err
			}
		default:
			if uint32(itemId) != targetTitleItemId {
				return schema_character.LimitBreakCharacterResponseSchema{}, errors.New("invalid item id")
			}
			if consumed := user.ConsumeItem(uint32(itemId), 1); !consumed {
				return schema_character.LimitBreakCharacterResponseSchema{}, err
			}
		}
	}

	for range itemIds {
		managedCharacter.IncreaseLevelBreakCount()
	}

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
	})
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	resp := schema_character.LimitBreakCharacterResponseSchema{
		ManagedCharacter: *managedCharacter,
		ItemSummary:      user.ItemSummary,
		Gold:             user.Gold,
	}
	return resp, nil
}

func (s *characterService) SetViewCharacter(internalUserId uint, managedCharacterIds []value_user.ManagedCharacterId, evolved []bool) ([]model_user.ManagedCharacter, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	// Update managed character view character id
	for i, managedCharacterId := range managedCharacterIds {
		managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
		if err != nil {
			return []model_user.ManagedCharacter{}, err
		}
		managedCharacter.ViewCharacterId = managedCharacter.CharacterId.SetEvoluteState(evolved[i])
	}
	// Save the changes
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	return user.ManagedCharacters, nil
}

func (s *characterService) ResetViewAllCharacter(internalUserId uint) ([]model_user.ManagedCharacter, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	// Reset all character's view character id
	for i := range user.ManagedCharacters {
		user.ManagedCharacters[i].ViewCharacterId = user.ManagedCharacters[i].CharacterId
	}
	// Save the changes
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	return user.ManagedCharacters, nil
}

func (s *characterService) SetShownCharacter(internalUserId uint, managedCharacterId value_user.ManagedCharacterId, shown bool) error {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return err
	}
	// Reset all character's view character id
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return err
	}
	// NOTE: Ignore shown param since there is no use case to un-shown it
	managedCharacter.Shown = 1
	// Save the changes
	_, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *characterService) SetSupportCharacters(internalUserId uint, req schema_character.SetSupportCharacterRequestSchema) error {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	// Set support characters
	for _, reqSupport := range req.SupportCharacters {
		supportCharacter, err := user.GetSupportCharacter(reqSupport.ManagedSupportId)
		if err != nil {
			return err
		}
		supportCharacter.Name = reqSupport.Name
		supportCharacter.Active = reqSupport.Active
		for i := range supportCharacter.ManagedWeaponIds {
			supportCharacter.ManagedWeaponIds[i].ManagedWeaponId = reqSupport.ManagedWeaponIds[i]
		}
		for i := range supportCharacter.ManagedCharacterIds {
			supportCharacter.ManagedCharacterIds[i].ManagedCharacterId = reqSupport.ManagedCharacterIds[i]
		}
	}
	// Save the changes
	_, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *characterService) SetSupportCharactersName(internalUserId uint, managedSupportId value_user.ManagedSupportId, name string) error {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	// Change the support name
	supportCharacter, err := user.GetSupportCharacter(managedSupportId)
	if err != nil {
		return err
	}
	supportCharacter.Name = name
	// Save the changes
	_, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	return nil
}
