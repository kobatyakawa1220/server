package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type PlayerFavoriteMemberService interface {
	GetFavoriteMember(internalUserId uint) ([]model_user.FavoriteMember, error)
}

type playerFavoriteMemberService struct {
	uu usecase.UserUsecase
}

func NewPlayerFavoriteMemberService(
	uu usecase.UserUsecase,
) PlayerFavoriteMemberService {
	return &playerFavoriteMemberService{uu}
}

func (s *playerFavoriteMemberService) GetFavoriteMember(internalUserId uint) ([]model_user.FavoriteMember, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{FavoriteMembers: true})
	if err != nil {
		return nil, err
	}
	return user.FavoriteMembers, nil
}
