package service

import (
	"reflect"
	"testing"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
)

// ":[{"facilityId":1,"actionNo":10,"openState":1,"nextLevel":1,"buildPointIndex":2,"actionTime":1677547022236,"buildTime":1677547022236},{"facilityId":113000,"actionNo":0,"openState":1,"nextLevel":1,"buildPointIndex":536870914,"actionTime":1677547022236,"buildTime":1677547022236}]}

func Test_userTownFacilityService_BuyTownFacility(t *testing.T) {
	logger := database.InitLogger()
	logRepo := database.InitLoggerRepo(logger)
	db := database.InitDatabase(logRepo, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	uu := InitializeUserUsecase(logRepo, db)
	s := InitializeUserTownFacilityService(logRepo, db)

	type args struct {
		internalUserId  uint
		actionTime      uint64
		buildTime       uint64
		facilityId      uint32
		nextLevel       uint8
		openState       int64
		buildPointIndex int64
	}
	tests := []struct {
		name                    string
		s                       UserTownFacilityService
		args                    args
		wantLastManagedFacility model_user.ManagedTownFacility
		wantErr                 bool
	}{
		{
			name: "buy town facility at tutorial success",
			s:    s,
			args: args{
				internalUserId:  1,
				facilityId:      113000,
				buildPointIndex: 536870914,
				nextLevel:       1,
				openState:       int64(value_town_facility.TownFacilityOpenStateOpen),
				actionTime:      1677547022236,
				buildTime:       1677547022236,
			},
			wantLastManagedFacility: model_user.ManagedTownFacility{
				ManagedTownFacilityId: 13,
				UserId:                2,
				FacilityId:            113000,
				BuildPointIndex:       536870914,
				Level:                 1,
				OpenState:             value_town_facility.TownFacilityOpenStateOpen,
				ActionTime:            1677547022236,
				BuildTime:             1677547022236,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := uu.CreateUser("test", "test", nil, nil, nil)
			if err != nil {
				t.Errorf("userRepo.CreateUser error = %v, wantErr nil", err)
			}
			got, _, err := tt.s.BuyTownFacility(u.Id, tt.args.actionTime, tt.args.buildTime, tt.args.facilityId, tt.args.nextLevel, tt.args.openState, tt.args.buildPointIndex)
			if (err != nil) != tt.wantErr {
				t.Errorf("userTownFacilityService.BuyTownFacility() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			lastManagedFacility := got.ManagedFacilities[len(got.ManagedFacilities)-1]
			if !reflect.DeepEqual(lastManagedFacility, tt.wantLastManagedFacility) {
				t.Errorf("userTownFacilityService.BuyTownFacility().ManagedFacilities[-1] = %v, want %v", lastManagedFacility, tt.wantLastManagedFacility)
			}
		})
	}
}
