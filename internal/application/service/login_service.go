package service

import (
	"encoding/json"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type UserLoginService interface {
	Login(deviceUUId string, sessionUUId string) (string, error)
}

type userLoginService struct {
	uu usecase.UserUsecase
	su usecase.ScheduleUsecase
}

func NewLoginService(
	uu usecase.UserUsecase,
	su usecase.ScheduleUsecase,
) UserLoginService {
	return &userLoginService{uu, su}
}

// FIXME: This method is TOO UN-EFFECTIVE
func (s *userLoginService) Login(deviceUUId string, sessionUUId string) (string, error) {
	user, err := s.uu.GetUserBySession(deviceUUId, sessionUUId)
	if err != nil {
		return "", err
	}
	user, err = s.uu.GetUserByInternalId(user.Id, repository.UserRepositoryParam{
		LoginBonuses:             true,
		ManagedFieldPartyMembers: true,
		ManagedTowns:             true,
	})
	if err != nil {
		return "", err
	}

	user.RefreshStamina()

	// Check last schedule refresh date
	lastLoginDay := user.LastLoginAt.Day()
	today := time.Now().Day()
	// Keep current schedule if the user logged in today
	if lastLoginDay == today {
		user.LastLoginBonus = nil
		if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{}); err != nil {
			return "", err
		}
		return user.Session, nil
	}

	// Receive login bonus (real process)
	received := user.ReceiveLoginBonuses()
	// Convert  to json strings for use at /login_bonus/get
	receivedJsonBytes, err := json.Marshal(received)
	if err != nil {
		return "", err
	}
	receivedJsonBytesString := string(receivedJsonBytes)
	user.LastLoginBonus = &receivedJsonBytesString

	// Recreate user schedule
	var characterIds []value_character.CharacterId
	for _, m := range user.ManagedFieldPartyMembers {
		characterId, err := value_character.NewCharacterId(uint32(m.CharacterId))
		if err != nil {
			return "", err
		}
		characterIds = append(characterIds, characterId)
	}
	gridData := user.ManagedTowns[0].GridData
	schedules, err := s.su.CreateSchedule(characterIds, gridData)
	if err != nil {
		return "", err
	}

	// Update field party schedules
	for i, schedule := range schedules {
		user.ManagedFieldPartyMembers[i].ScheduleTable = &schedule.ScheduleTable
		// TODO: Save the item drop (it will be used at /reap_drop)
		user.ManagedFieldPartyMembers[i].PartyDropPresents = nil
	}
	if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedFieldPartyMembers: true,
		LoginBonuses:             true,
	}); err != nil {
		return "", err
	}

	return user.Session, nil
}
