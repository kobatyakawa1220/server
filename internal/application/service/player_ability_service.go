package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type PlayerAbilityService interface {
	// Equip Player Ability
	EquipPlayerAbility(internalUserId uint, managedAbilityBoardId int64, slotIndex uint8, itemId int64) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, error)
	// Release Player Ability
	ReleasePlayerAbility(internalUserId uint, itemId int64, managedCharacterId int64) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, error)
	// Release Slot Player Ability
	ReleaseSlotPlayerAbility(internalUserId uint, managedAbilityBoardId int64, slotIndex uint8) (model_user.User, error)
	// Upgrade Sphere Player Ability
	UpgradeSpherePlayerAbility(
		internalUserId uint,
		managedAbilityBoardId int64,
		slotIndex uint8,
		materialItemIds []int64,
		materialItemAmounts []int64,
		srcItemId int64,
		amount int64,
	) (model_user.User, error)
}

type playerAbilityService struct {
	uu usecase.UserUsecase
}

func NewPlayerAbilityService(uu usecase.UserUsecase) PlayerAbilityService {
	return &playerAbilityService{uu}
}

func (s *playerAbilityService) EquipPlayerAbility(internalUserId uint, managedAbilityBoardId int64, slotIndex uint8, itemId int64) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, error) {
	return []model_user.ManagedAbilityBoard{}, []model_user.ItemSummary{}, errors.New("not implemented")
}

func (s *playerAbilityService) ReleasePlayerAbility(internalUserId uint, itemId int64, managedCharacterId int64) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, error) {
	return []model_user.ManagedAbilityBoard{}, []model_user.ItemSummary{}, errors.New("not implemented")
}

func (s *playerAbilityService) ReleaseSlotPlayerAbility(internalUserId uint, managedAbilityBoardId int64, slotIndex uint8) (model_user.User, error) {
	return model_user.User{}, errors.New("not implemented")
}

func (s *playerAbilityService) UpgradeSpherePlayerAbility(
	internalUserId uint,
	managedAbilityBoardId int64,
	slotIndex uint8,
	materialItemIds []int64,
	materialItemAmounts []int64,
	srcItemId int64,
	amount int64,
) (model_user.User, error) {
	return model_user.User{}, errors.New("not implemented")
}
