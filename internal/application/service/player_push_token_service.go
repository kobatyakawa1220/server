package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type PlayerPushTokenService interface {
	SetPlayerPushToken(internalUserId uint, token string) error
}

type playerPushTokenService struct {
	uu usecase.UserUsecase
}

func NewPlayerPushTokenService(uu usecase.UserUsecase) PlayerPushTokenService {
	return &playerPushTokenService{uu}
}

func (s *playerPushTokenService) SetPlayerPushToken(internalUserId uint, token string) error {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}

	user.PushToken = token

	if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{}); err != nil {
		return err
	}
	return nil
}
