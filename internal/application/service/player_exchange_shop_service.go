package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_exchange_shop "gitlab.com/kirafan/sparkle/server/internal/domain/model/exchange_shop"
)

type PlayerExchangeShopService interface {
	ShownPlayerExchangeShop(internalUserId uint, ExchangeShopIds []int64) ([]model_exchange_shop.ExchangeShop, error)
}

type playerExchangeShopService struct {
	uu usecase.UserUsecase
}

func NewPlayerExchangeShopService(uu usecase.UserUsecase) PlayerExchangeShopService {
	return &playerExchangeShopService{uu}
}

func (s *playerExchangeShopService) ShownPlayerExchangeShop(internalUserId uint, ExchangeShopIds []int64) ([]model_exchange_shop.ExchangeShop, error) {
	// TODO: This endpoint should change `/api/player/store/get_all` endpoint's exchangeShops field
	// STUB
	resp := []model_exchange_shop.ExchangeShop{}
	return resp, nil
}
