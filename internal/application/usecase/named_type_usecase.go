package usecase

import (
	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type NamedTypeUsecase interface {
	GetNamedTypeById(id uint) (*model_named_type.NamedType, error)
}

type namedTypeUsecase struct {
	rp     repository.NamedTypeRepository
	logger repository.LoggerRepository
}

func NewNamedTypeUsecase(rp repository.NamedTypeRepository, logger repository.LoggerRepository) NamedTypeUsecase {
	return &namedTypeUsecase{rp, logger}
}

func (uc *namedTypeUsecase) GetNamedTypeById(id uint) (*model_named_type.NamedType, error) {
	namedType, err := uc.rp.FindNamedType(&model_named_type.NamedType{NamedType: uint(id)}, nil, nil)
	if err != nil {
		return nil, err
	}
	return namedType, nil
}
