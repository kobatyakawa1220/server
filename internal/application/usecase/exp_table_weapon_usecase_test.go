package usecase

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_expTableWeaponUsecase_GetNextExpTableWeapon(t *testing.T) {
	p := persistence.NewExpTableWeaponRepositoryImpl(db)
	u := NewExpTableWeaponUsecase(p, logRepo)

	tests := []struct {
		name       string
		currentExp value_exp.WeaponExp
		weaponType uint8
		want       *model_exp_table.ExpTableWeapon
		wantErr    bool
	}{
		{
			name:       "get next exp table weapon 0 returns level 1 at exp 0",
			currentExp: 0,
			weaponType: 0,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          0,
				Level:               1,
				NextExp:             50,
				TotalExp:            50,
				RequiredCoinPerItem: 100,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 0 returns level 2 at exp 51",
			currentExp: 51,
			weaponType: 0,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          0,
				Level:               2,
				NextExp:             100,
				TotalExp:            150,
				RequiredCoinPerItem: 120,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 0 returns level 20 at exp 25151",
			currentExp: 25151,
			weaponType: 0,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          0,
				Level:               20,
				NextExp:             999999,
				TotalExp:            1025149,
				RequiredCoinPerItem: 0,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 1 returns level 1 at exp 0",
			currentExp: 0,
			weaponType: 1,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          1,
				Level:               1,
				NextExp:             150,
				TotalExp:            150,
				RequiredCoinPerItem: 50,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 1 returns level 2 at exp 151",
			currentExp: 151,
			weaponType: 1,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          1,
				Level:               2,
				NextExp:             450,
				TotalExp:            600,
				RequiredCoinPerItem: 150,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 1 returns level 20 at exp 54151",
			currentExp: 54151,
			weaponType: 1,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          1,
				Level:               20,
				NextExp:             999999,
				TotalExp:            1054149,
				RequiredCoinPerItem: 0,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 2 returns level 1 at exp 0",
			currentExp: 0,
			weaponType: 2,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          2,
				Level:               1,
				NextExp:             18000,
				TotalExp:            18000,
				RequiredCoinPerItem: 100,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 2 returns level 2 at exp 18001",
			currentExp: 18001,
			weaponType: 2,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          2,
				Level:               2,
				NextExp:             18540,
				TotalExp:            36540,
				RequiredCoinPerItem: 115,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table weapon 2 returns level 100 at exp 6765301",
			currentExp: 6765301,
			weaponType: 2,
			want: &model_exp_table.ExpTableWeapon{
				WeaponType:          2,
				Level:               100,
				NextExp:             999999,
				TotalExp:            7765299,
				RequiredCoinPerItem: 0,
			},
			wantErr: false,
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_exp_table.ExpTableWeapon{}, "ExpTableWeaponId"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := u.GetNextExpTableWeapon(tt.currentExp, tt.weaponType)
			if (err != nil) != tt.wantErr {
				t.Errorf("expTableWeaponUsecase.GetNextExpTableWeapon() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if cmp.Equal(got, tt.want, opts...) != true {
				t.Errorf("expTableWeaponUsecase.GetNextExpTableWeapon()  Diff = %+v", cmp.Diff(got, tt.want, opts...))
			}
		})
	}
}
