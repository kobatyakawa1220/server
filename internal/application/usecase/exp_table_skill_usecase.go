package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
)

type ExpTableSkillUsecase interface {
	GetNextExpTableSkill(currentExp uint32, skillType value_exp.ExpTableSkillType) (*model_exp_table.ExpTableSkill, error)
}

type expTableSkillUsecase struct {
	rp     repository.ExpTableSkillRepository
	logger repository.LoggerRepository
}

func NewExpTableSkillUsecase(rp repository.ExpTableSkillRepository, logger repository.LoggerRepository) ExpTableSkillUsecase {
	return &expTableSkillUsecase{rp, logger}
}

func (uc *expTableSkillUsecase) GetNextExpTableSkill(currentExp uint32, skillType value_exp.ExpTableSkillType) (*model_exp_table.ExpTableSkill, error) {
	criteria := map[string]interface{}{
		"total_exp >= ?": currentExp,
		"skill_type = ?": skillType,
	}
	expTableSkill, err := uc.rp.FindExpTableSkill(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableSkill, nil
}
