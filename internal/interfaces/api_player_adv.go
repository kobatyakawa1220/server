/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"encoding/json"
	"net/http"
	"strings"
)

// PlayerAdvApiController binds http requests to an api service and writes the service results to the http response
type PlayerAdvApiController struct {
	service      PlayerAdvApiServicer
	errorHandler ErrorHandler
}

// PlayerAdvApiOption for how the controller is set up.
type PlayerAdvApiOption func(*PlayerAdvApiController)

// WithPlayerAdvApiErrorHandler inject ErrorHandler into controller
func WithPlayerAdvApiErrorHandler(h ErrorHandler) PlayerAdvApiOption {
	return func(c *PlayerAdvApiController) {
		c.errorHandler = h
	}
}

// NewPlayerAdvApiController creates a default api controller
func NewPlayerAdvApiController(s PlayerAdvApiServicer, opts ...PlayerAdvApiOption) Router {
	controller := &PlayerAdvApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}

	for _, opt := range opts {
		opt(controller)
	}

	return controller
}

// Routes returns all the api routes for the PlayerAdvApiController
func (c *PlayerAdvApiController) Routes() Routes {
	return Routes{
		{
			"AddPlayerAdv",
			strings.ToUpper("Post"),
			"/api/player/adv/add",
			c.AddPlayerAdv,
		},
	}
}

// AddPlayerAdv - Add Player Adv (WIP)
func (c *PlayerAdvApiController) AddPlayerAdv(w http.ResponseWriter, r *http.Request) {
	addPlayerAdvRequestParam := AddPlayerAdvRequest{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&addPlayerAdvRequestParam); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertAddPlayerAdvRequestRequired(addPlayerAdvRequestParam); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.AddPlayerAdv(r.Context(), addPlayerAdvRequestParam)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}
