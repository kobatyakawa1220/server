/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx_func"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerChestApiService is a service that implements the logic for the PlayerChestApiServicer
// This service should implement the business logic for every endpoint for the PlayerChestApi API.
// Include any external packages or services that will be required by this service.
type PlayerChestApiService struct {
	cs service.PlayerChestService
}

// NewPlayerChestApiService creates a default api service
func NewPlayerChestApiService(cs service.PlayerChestService) PlayerChestApiServicer {
	return &PlayerChestApiService{cs}
}

// DrawPlayerChest - Draw Player Chest (WIP)
func (s *PlayerChestApiService) DrawPlayerChest(ctx context.Context, req DrawPlayerChestRequest) (ImplResponse, error) {
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	user, chest, prizes, err := s.cs.DrawPlayerChest(internalId, uint(req.ChestId), uint8(req.Count))
	if err != nil {
		// STUB
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_UNAVAILABLE)), nil
	}

	var outPrizes []PrizeResultsArrayObject
	calc.Copy(&outPrizes, &prizes)
	var outChest DrawPlayerChestResponseChest
	calc.Copy(&outChest, &chest)
	calc.Copy(&outChest.ResetChestPrizes, &chest.ResetChestPrizes)
	success := response.NewSuccessResponse()
	return Response(http.StatusOK, DrawPlayerChestResponse{
		Chest:              outChest,
		PrizeResults:       outPrizes,
		Player:             ToBasePlayerResponse(&user),
		ItemSummary:        toItemSummaryResponse(user.ItemSummary),
		ManagedRoomObjects: ToManagedRoomObjectsResponse(user.ManagedRoomObjects),
		// TODO
		ManagedWeapons: []ManagedWeaponsArrayObject{},
		// TODO
		AddPresentAmount:    0,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// GetAllPlayerChest - Get All Player Chest (WIP)
func (s *PlayerChestApiService) GetAllPlayerChest(ctx context.Context) (ImplResponse, error) {
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	chests, err := s.cs.GetAllPlayerChest(internalId)
	if err != nil {
		// STUB
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_UNAVAILABLE)), nil
	}

	var outChests []ResetPlayerChestResponseChest
	calc.Copy(&outChests, &chests)
	for i := range outChests {
		calc.Copy(&outChests[i].ResetChestPrizes, &chests[i].ResetChestPrizes)
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetAllPlayerChestResponse{
		Chests:              outChests,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// GetStepPlayerChest - Get Step Player Chest (WIP)
func (s *PlayerChestApiService) GetStepPlayerChest(ctx context.Context, req GetStepPlayerChestRequest) (ImplResponse, error) {
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	prizes, err := s.cs.GetStepPlayerChest(internalId, int(req.ChestId), uint16(req.Step))
	if err != nil {
		// STUB
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_UNAVAILABLE)), nil
	}

	var outPrizes []ResetChestPrizesArrayObject
	calc.Copy(&outPrizes, &prizes)
	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetStepPlayerChestResponse{
		Prizes:              outPrizes,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// ResetPlayerChest - Reset Player Chest (WIP)
func (s *PlayerChestApiService) ResetPlayerChest(ctx context.Context, req ResetPlayerChestRequest) (ImplResponse, error) {
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	chest, err := s.cs.ResetPlayerChest(internalId, int(req.ChestId))
	if err != nil {
		// STUB
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_UNAVAILABLE)), nil
	}

	var outChest ResetPlayerChestResponseChest
	calc.Copy(&outChest, &chest)
	calc.Copy(&outChest.ResetChestPrizes, &chest.ResetChestPrizes)

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, ResetPlayerChestResponse{
		Chest:               outChest,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}
