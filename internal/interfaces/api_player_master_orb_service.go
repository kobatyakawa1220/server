/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerMasterOrbApiService is a service that implements the logic for the PlayerMasterOrbApiServicer
// This service should implement the business logic for every endpoint for the PlayerMasterOrbApi API.
// Include any external packages or services that will be required by this service.
type PlayerMasterOrbApiService struct {
}

// NewPlayerMasterOrbApiService creates a default api service
func NewPlayerMasterOrbApiService() PlayerMasterOrbApiServicer {
	return &PlayerMasterOrbApiService{}
}

// GetAllPlayerMasterOrb - Get All Player Master Orb (WIP)
func (s *PlayerMasterOrbApiService) GetAllPlayerMasterOrb(ctx context.Context) (ImplResponse, error) {
	// TODO - update GetAllPlayerMasterOrb with the required logic for this service method.
	// Add api_player_master_orb_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(http.StatusOK, GetAllPlayerMasterOrbResponse{}) or use other options such as http.Ok ...
	//return Response(http.StatusOK, GetAllPlayerMasterOrbResponse{}), nil

	// STUB
	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetAllPlayerMasterOrbResponse{
		ManagedMasterOrbs:   []ManagedMasterOrbsArrayObject{},
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}
