/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type BonusItemsArrayObject struct {

	// Item count of receive
	Amount int64 `json:"amount"`

	// Unknown 30001 if bonus is specified
	MessageMakeId int64 `json:"messageMakeId"`

	// ItemId if type is 2 otherwise -1
	ObjId int64 `json:"objId"`

	// 2:Gem / 10:Item / 0:None
	Type int64 `json:"type"`
}

// AssertBonusItemsArrayObjectRequired checks if the required fields are not zero-ed
func AssertBonusItemsArrayObjectRequired(obj BonusItemsArrayObject) error {
	elements := map[string]interface{}{
		"amount":        obj.Amount,
		"messageMakeId": obj.MessageMakeId,
		"objId":         obj.ObjId,
		"type":          obj.Type,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseBonusItemsArrayObjectRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of BonusItemsArrayObject (e.g. [][]BonusItemsArrayObject), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseBonusItemsArrayObjectRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aBonusItemsArrayObject, ok := obj.(BonusItemsArrayObject)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertBonusItemsArrayObjectRequired(aBonusItemsArrayObject)
	})
}
