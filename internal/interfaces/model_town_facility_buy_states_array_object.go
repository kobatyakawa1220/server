/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type TownFacilityBuyStatesArrayObject struct {
	ActionNo int64 `json:"actionNo"`

	ActionTime int64 `json:"actionTime"`

	BuildPointIndex int64 `json:"buildPointIndex"`

	BuildTime int64 `json:"buildTime"`

	FacilityId int64 `json:"facilityId"`

	NextLevel int64 `json:"nextLevel"`

	OpenState int64 `json:"openState"`
}

// AssertTownFacilityBuyStatesArrayObjectRequired checks if the required fields are not zero-ed
func AssertTownFacilityBuyStatesArrayObjectRequired(obj TownFacilityBuyStatesArrayObject) error {
	elements := map[string]interface{}{
		"actionTime":      obj.ActionTime,
		"buildPointIndex": obj.BuildPointIndex,
		"buildTime":       obj.BuildTime,
		"facilityId":      obj.FacilityId,
		"nextLevel":       obj.NextLevel,
		"openState":       obj.OpenState,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseTownFacilityBuyStatesArrayObjectRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of TownFacilityBuyStatesArrayObject (e.g. [][]TownFacilityBuyStatesArrayObject), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseTownFacilityBuyStatesArrayObjectRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aTownFacilityBuyStatesArrayObject, ok := obj.(TownFacilityBuyStatesArrayObject)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertTownFacilityBuyStatesArrayObjectRequired(aTownFacilityBuyStatesArrayObject)
	})
}
