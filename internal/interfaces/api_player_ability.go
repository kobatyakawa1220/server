/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"encoding/json"
	"net/http"
	"strings"
)

// PlayerAbilityApiController binds http requests to an api service and writes the service results to the http response
type PlayerAbilityApiController struct {
	service      PlayerAbilityApiServicer
	errorHandler ErrorHandler
}

// PlayerAbilityApiOption for how the controller is set up.
type PlayerAbilityApiOption func(*PlayerAbilityApiController)

// WithPlayerAbilityApiErrorHandler inject ErrorHandler into controller
func WithPlayerAbilityApiErrorHandler(h ErrorHandler) PlayerAbilityApiOption {
	return func(c *PlayerAbilityApiController) {
		c.errorHandler = h
	}
}

// NewPlayerAbilityApiController creates a default api controller
func NewPlayerAbilityApiController(s PlayerAbilityApiServicer, opts ...PlayerAbilityApiOption) Router {
	controller := &PlayerAbilityApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}

	for _, opt := range opts {
		opt(controller)
	}

	return controller
}

// Routes returns all the api routes for the PlayerAbilityApiController
func (c *PlayerAbilityApiController) Routes() Routes {
	return Routes{
		{
			"EquipPlayerAbility",
			strings.ToUpper("Post"),
			"/api/player/ability/equip",
			c.EquipPlayerAbility,
		},
		{
			"ReleasePlayerAbility",
			strings.ToUpper("Post"),
			"/api/player/ability/release",
			c.ReleasePlayerAbility,
		},
		{
			"ReleaseSlotPlayerAbility",
			strings.ToUpper("Post"),
			"/api/player/ability/release_slot",
			c.ReleaseSlotPlayerAbility,
		},
		{
			"UpgradeSpherePlayerAbility",
			strings.ToUpper("Post"),
			"/api/player/ability/upgrade_sphere",
			c.UpgradeSpherePlayerAbility,
		},
	}
}

// EquipPlayerAbility - Equip Player Ability (WIP)
func (c *PlayerAbilityApiController) EquipPlayerAbility(w http.ResponseWriter, r *http.Request) {
	equipPlayerAbilityRequestParam := EquipPlayerAbilityRequest{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&equipPlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertEquipPlayerAbilityRequestRequired(equipPlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.EquipPlayerAbility(r.Context(), equipPlayerAbilityRequestParam)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

// ReleasePlayerAbility - Release Player Ability (WIP)
func (c *PlayerAbilityApiController) ReleasePlayerAbility(w http.ResponseWriter, r *http.Request) {
	releasePlayerAbilityRequestParam := ReleasePlayerAbilityRequest{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&releasePlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertReleasePlayerAbilityRequestRequired(releasePlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.ReleasePlayerAbility(r.Context(), releasePlayerAbilityRequestParam)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

// ReleaseSlotPlayerAbility - Release Slot Player Ability (WIP)
func (c *PlayerAbilityApiController) ReleaseSlotPlayerAbility(w http.ResponseWriter, r *http.Request) {
	releaseSlotPlayerAbilityRequestParam := ReleaseSlotPlayerAbilityRequest{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&releaseSlotPlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertReleaseSlotPlayerAbilityRequestRequired(releaseSlotPlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.ReleaseSlotPlayerAbility(r.Context(), releaseSlotPlayerAbilityRequestParam)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

// UpgradeSpherePlayerAbility - Upgrade Sphere Player Ability (WIP)
func (c *PlayerAbilityApiController) UpgradeSpherePlayerAbility(w http.ResponseWriter, r *http.Request) {
	upgradeSpherePlayerAbilityRequestParam := UpgradeSpherePlayerAbilityRequest{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&upgradeSpherePlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertUpgradeSpherePlayerAbilityRequestRequired(upgradeSpherePlayerAbilityRequestParam); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.UpgradeSpherePlayerAbility(r.Context(), upgradeSpherePlayerAbilityRequestParam)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}
