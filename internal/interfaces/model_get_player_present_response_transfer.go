package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetPlayerPresentResponse(
	user *model_user.User,
	presents *[]model_user.UserPresent,
	success *response.BaseResponse,
) *GetPlayerPresentResponse {
	var outReceived []ReceivedArrayObject
	for _, present := range *presents {
		base := ReceivedArrayObject{
			ManagedPresentId: int64(present.ManagedPresentId),
			PlayerId:         int64(user.Id),
			ObjectId:         int64(present.Present.ObjectId),
			Amount:           int64(present.Amount),
			Title:            present.Present.Title,
			Message:          present.Present.Message,
			Options:          present.Present.Options,
			Type:             int64(present.Type),
			CreatedAt:        response.ToSparkleTime(present.CreatedAt),
			// NOTE: IDK but this string has timezone
			ReceivedAt:   response.NewSparkleTime(),
			DeadlineAt:   response.ToSparkleTime(present.DeadlineAt),
			DeadlineFlag: present.Present.DeadlineFlag,
			Source:       int64(present.Present.Source),
		}
		if present.ReceivedAt != nil {
			base.ReceivedAt = response.ToSparkleTime(*present.ReceivedAt)
		}
		outReceived = append(outReceived, base)
	}

	var outBasePlayer BasePlayer
	calc.Copy(&outBasePlayer, &user)
	outBasePlayer.CreatedAt = response.ToSparkleTime(user.CreatedAt)
	outBasePlayer.LastLoginAt = response.ToSparkleTime(user.LastLoginAt)
	outBasePlayer.StaminaUpdatedAt = response.ToSparkleTime(user.StaminaUpdatedAt)
	if user.LastPartyAdded != nil {
		outBasePlayer.LastPartyAdded = response.ToSparkleTime(*user.LastPartyAdded)
	} else {
		outBasePlayer.LastPartyAdded = "0001-01-01T00:00:00"
	}

	var outItemSummary []ItemSummaryArrayObject
	calc.Copy(&outItemSummary, &user.ItemSummary)
	var outManagedCharacters []ManagedCharactersArrayObject
	calc.Copy(&outManagedCharacters, &user.ManagedCharacters)
	var outManagedMasterOrbs []ManagedMasterOrbsArrayObject
	calc.Copy(&outManagedMasterOrbs, &user.ManagedMasterOrbs)
	var outManagedNamedTypes []ManagedNamedTypesArrayObject
	calc.Copy(&outManagedNamedTypes, &user.ManagedNamedTypes)
	var outManagedRoomObjects []ManagedRoomObjectsArrayObject
	calc.Copy(&outManagedRoomObjects, &user.ManagedRoomObjects)
	var outManagedTownFacilities []ManagedTownFacilitiesArrayObject
	calc.Copy(&outManagedTownFacilities, &user.ManagedFacilities)
	outManagedWeapons := make([]ManagedWeaponsArrayObject, 0)
	calc.Copy(&outManagedWeapons, &user.ManagedWeapons)
	var outOfferTitleTypes []OfferTitleTypesArrayObject
	calc.Copy(&outOfferTitleTypes, &user.OfferTitleTypes)

	offers := make([]OffersArrayObject, 0)

	return &GetPlayerPresentResponse{
		// NOTE: Since it is lazy to implement (and only affect to client-side screen effect) this got skipped
		ArousalResults:        []ArousalResultsArrayObject{},
		Received:              outReceived,
		ItemSummary:           outItemSummary,
		ManagedCharacters:     outManagedCharacters,
		ManagedMasterOrbs:     outManagedMasterOrbs,
		ManagedNamedTypes:     outManagedNamedTypes,
		ManagedRoomObjects:    outManagedRoomObjects,
		ManagedTownFacilities: outManagedTownFacilities,
		ManagedWeapons:        outManagedWeapons,
		OfferTitleTypes:       outOfferTitleTypes,
		// TODO: Implement crea-craft thing
		Offers: &offers,
		Player: outBasePlayer,
		// template
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
