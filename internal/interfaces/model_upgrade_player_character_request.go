/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type UpgradePlayerCharacterRequest struct {
	Amount string `json:"amount"`

	ItemId string `json:"itemId"`

	ManagedCharacterId int64 `json:"managedCharacterId"`
}

// AssertUpgradePlayerCharacterRequestRequired checks if the required fields are not zero-ed
func AssertUpgradePlayerCharacterRequestRequired(obj UpgradePlayerCharacterRequest) error {
	elements := map[string]interface{}{
		"amount":             obj.Amount,
		"itemId":             obj.ItemId,
		"managedCharacterId": obj.ManagedCharacterId,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseUpgradePlayerCharacterRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of UpgradePlayerCharacterRequest (e.g. [][]UpgradePlayerCharacterRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseUpgradePlayerCharacterRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aUpgradePlayerCharacterRequest, ok := obj.(UpgradePlayerCharacterRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertUpgradePlayerCharacterRequestRequired(aUpgradePlayerCharacterRequest)
	})
}
