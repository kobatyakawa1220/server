package persistence

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type weaponRecipeRepositoryImpl struct {
	Conn *gorm.DB
}

func NewWeaponRecipeRepositoryImpl(conn *gorm.DB) repository.WeaponRecipeRepository {
	return &weaponRecipeRepositoryImpl{Conn: conn}
}

func (rp *weaponRecipeRepositoryImpl) FindByRecipeId(recipeId uint32) (*model_weapon.WeaponRecipe, error) {
	var data *model_weapon.WeaponRecipe
	result := rp.Conn.Preload("RecipeMaterials").Where("recipe_id = ?", recipeId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
