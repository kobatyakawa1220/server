package persistence

import (
	"reflect"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type userRepositoryImpl struct {
	Conn *gorm.DB
}

// NewUserRepositoryImpl user repositoryのコンストラクタ
func NewUserRepositoryImpl(conn *gorm.DB) repository.UserRepository {
	return &userRepositoryImpl{Conn: conn}
}

// Create userの保存
func (ur *userRepositoryImpl) CreateUser(user *model_user.User) (*model_user.User, error) {
	if err := ur.Conn.Create(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

// Update userの更新
func (ur *userRepositoryImpl) UpdateUser(user *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error) {
	if param.Entire {
		if err := ur.Conn.Session(&gorm.Session{FullSaveAssociations: true}).Save(&user).Error; err != nil {
			return nil, err
		}
		return user, nil
	}

	tx := ur.Conn.Begin()

	// Loop through all fields in the param struct
	for field, value := range map[string]interface{}{
		"ItemSummary":              &model_user.ItemSummary{},
		"SupportCharacters":        &model_user.SupportCharacter{},
		"ManagedBattleParties":     &model_user.ManagedBattleParty{},
		"ManagedCharacters":        &model_user.ManagedCharacter{},
		"ManagedNamedTypes":        &model_user.ManagedNamedType{},
		"ManagedFieldPartyMembers": &model_user.ManagedFieldPartyMember{},
		"ManagedTowns":             &model_user.ManagedTown{},
		"ManagedFacilities":        &model_user.ManagedTownFacility{},
		"ManagedRoomObjects":       &model_user.ManagedRoomObject{},
		"ManagedWeapons":           &model_user.ManagedWeapon{},
		"ManagedRooms":             &model_user.ManagedRoom{},
		"ManagedMasterOrbs":        &model_user.ManagedMasterOrb{},
		"ManagedAbilityBoards":     &model_user.ManagedAbilityBoard{},
		"FavoriteMembers":          &model_user.FavoriteMember{},
		"OfferTitleTypes":          &model_user.OfferTitleType{},
		"AdvIds":                   &model_user.ClearedAdvId{},
		"TipIds":                   &model_user.ShownTipId{},
		"QuestLogs":                &model_user.QuestLog{},
		"QuestAccesses":            &model_user.QuestAccess{},
		"Missions":                 &model_user.UserMission{},
		"Presents":                 &model_user.UserPresent{},
		"Achievements":             &model_user.UserAchievement{},
		"Gachas":                   &model_user.UserGacha{},
		"LoginBonuses":             &model_user.UserLoginBonus{},
	} {
		// If the field is set to true in the param struct, replace the association
		if reflect.ValueOf(param).FieldByName(field).Bool() {
			switch field {
			case "ManagedCharacters":
				if deleteErr := tx.Unscoped().Delete(value, "player_id = ?", user.Id).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "ManagedRooms":
				// FIXME: Maybe there is more smart way...
				managedRoomIDs := make([]uint, len(user.ManagedRooms))
				for i := range user.ManagedRooms {
					managedRoomIDs[i] = user.ManagedRooms[i].ManagedRoomId
				}
				if deleteErr := tx.Unscoped().Where("managed_room_id IN (?)", managedRoomIDs).Delete(&model_user.ManagedRoomArrangeData{}).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
				if deleteErr := tx.Unscoped().Where("managed_room_id IN (?)", managedRoomIDs).Delete(&model_user.ManagedRoom{}).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "SupportCharacters":
				// FIXME: Maybe there is more smart way...
				managedSupportIds := make([]value_user.ManagedSupportId, len(user.SupportCharacters))
				for i := range user.SupportCharacters {
					managedSupportIds[i] = user.SupportCharacters[i].ManagedSupportId
				}
				if deleteErr := tx.Unscoped().Where("managed_support_id IN (?)", managedSupportIds).Delete(&model_user.SupportCharacterManagedCharacterId{}).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
				if deleteErr := tx.Unscoped().Where("managed_support_id IN (?)", managedSupportIds).Delete(&model_user.SupportCharacterManagedWeaponId{}).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			default:
				if deleteErr := tx.Unscoped().Delete(value, "user_id = ?", user.Id).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			}
		}
	}

	if err := tx.Save(&user).Error; err != nil {
		return nil, err
	}
	tx.Commit()
	return user, nil
}

func (ur *userRepositoryImpl) FindUserByQuery(query *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error) {
	var user *model_user.User
	chain := ur.Conn

	if param.Entire {
		chain = chain.Preload(clause.Associations)
		chain = chain.Preload("SupportCharacters.ManagedCharacterIds")
		chain = chain.Preload("SupportCharacters.ManagedWeaponIds")
		chain = chain.Preload("ManagedRooms.ArrangeData")
		chain = chain.Preload("LoginBonuses.LoginBonus")
		chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays")
		chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays.BonusItems")
		chain = chain.Preload("Missions")
		chain = chain.Preload("Missions.Mission.Reward")
		if result := chain.Where(query).First(&user); result.Error != nil {
			return nil, result.Error
		}
		return user, nil
	}

	// Define a slice of field names
	fields := []string{
		"ItemSummary",
		"SupportCharacters",
		"ManagedBattleParties",
		"ManagedCharacters",
		"ManagedNamedTypes",
		"ManagedFieldPartyMembers",
		"ManagedTowns",
		"ManagedFacilities",
		"ManagedRoomObjects",
		"ManagedWeapons",
		"ManagedRooms",
		"ManagedMasterOrbs",
		"ManagedAbilityBoards",
		"FavoriteMembers",
		"OfferTitleTypes",
		"AdvIds",
		"TipIds",
		"QuestLogs",
		"QuestAccesses",
		"Missions",
		"Presents",
		"Achievements",
		"Gachas",
		"LoginBonuses",
	}
	// Loop through the fields of the param struct and preload associations as needed
	for _, field := range fields {
		if reflect.ValueOf(param).FieldByName(field).Bool() {
			switch field {
			case "SupportCharacters":
				chain = chain.Preload("SupportCharacters")
				chain = chain.Preload("SupportCharacters.ManagedCharacterIds")
				chain = chain.Preload("SupportCharacters.ManagedWeaponIds")
			case "ManagedRooms":
				chain = chain.Preload("ManagedRooms")
				chain = chain.Preload("ManagedRooms.ArrangeData")
			case "LoginBonuses":
				chain = chain.Preload("LoginBonuses")
				chain = chain.Preload("LoginBonuses.LoginBonus")
				chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays")
				chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays.BonusItems")
			case "Gachas":
				chain = chain.Preload("Gachas")
				chain = chain.Preload("Gachas.Gacha")
				chain = chain.Preload("Gachas.Gacha.SelectionCharacterIds")
				chain = chain.Preload("Gachas.Gacha.DrawPoints")
				chain = chain.Preload("Gachas.Gacha.GachaSteps")
				chain = chain.Preload("Gachas.Gacha.GachaSteps.GachaBonusItems")
			case "Presents":
				chain = chain.Preload("Presents")
				chain = chain.Preload("Presents.Present")
			case "Missions":
				chain = chain.Preload("Missions")
				chain = chain.Preload("Missions.Mission")
				chain = chain.Preload("Missions.Mission.Reward")
			case "Achievements":
				chain = chain.Preload("Achievements")
				chain = chain.Preload("Achievements.Achievement")
			default:
				chain = chain.Preload(field)
			}
		}
	}
	if result := chain.Where(query).First(&user); result.Error != nil {
		return nil, result.Error
	}
	return user, nil
}

func (ur *userRepositoryImpl) FindUserById(id uint, param repository.UserRepositoryParam) (*model_user.User, error) {
	query := &model_user.User{Id: id}
	return ur.FindUserByQuery(query, param)
}

func (ur *userRepositoryImpl) FindUserByUUID(UUId string, param repository.UserRepositoryParam) (*model_user.User, error) {
	query := &model_user.User{UUId: UUId}
	return ur.FindUserByQuery(query, param)
}

func (ur *userRepositoryImpl) FindUserBySession(deviceUUId string, sessionUUId string) (*model_user.User, error) {
	query := &model_user.User{UUId: deviceUUId, Session: sessionUUId}
	return ur.FindUserByQuery(query, repository.UserRepositoryParam{})
}

func (ur *userRepositoryImpl) FindUserByFriendCode(code string) (*model_user.User, error) {
	query := &model_user.User{MyCode: code}
	return ur.FindUserByQuery(query, repository.UserRepositoryParam{})
}

func (ur *userRepositoryImpl) FindUserByMoveCode(code string) (*model_user.User, error) {
	query := &model_user.User{MoveCode: code}
	return ur.FindUserByQuery(query, repository.UserRepositoryParam{})
}

func (ur *userRepositoryImpl) FindUserQuestLog(userId uint, query *model_user.QuestLog, order *string) (*model_user.QuestLog, error) {
	var questLog *model_user.QuestLog
	var result *gorm.DB
	chain := ur.Conn.Model(&query)
	if order != nil {
		chain = chain.Order(*order)
	}
	result = chain.Where("user_id = ?", userId).Where(query).First(&questLog)
	if result.Error != nil {
		return nil, result.Error
	}
	return questLog, nil
}

func (ur *userRepositoryImpl) UpdateUserQuestLog(questLog *model_user.QuestLog) (*model_user.QuestLog, error) {
	if err := ur.Conn.Save(&questLog).Error; err != nil {
		return nil, err
	}
	return questLog, nil
}

func (ur *userRepositoryImpl) InsertUserQuestLog(userId uint, questLog *model_user.QuestLog) (*model_user.QuestLog, error) {
	user := model_user.User{Id: userId}
	if err := ur.Conn.Model(&user).Association("QuestLogs").Append([]model_user.QuestLog{*questLog}); err != nil {
		return nil, err
	}
	return questLog, nil
}

func (ur *userRepositoryImpl) GetUserQuestLogClearRanks(userId uint) ([]*model_user.QuestClearRank, error) {
	var questClearRanks []*model_user.QuestClearRank
	if result := ur.Conn.Model(&model_user.QuestLog{}).Where(
		"user_id = ?", userId,
	).Select(
		[]string{"quest_id", "MAX(clear_rank) as clear_rank"},
	).Group(
		"quest_id",
	).Find(&questClearRanks); result.Error != nil {
		return nil, result.Error
	}
	return questClearRanks, nil
}
