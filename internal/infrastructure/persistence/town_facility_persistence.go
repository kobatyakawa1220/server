package persistence

import (
	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type townFacilityRepositoryImpl struct {
	Conn *gorm.DB
}

func NewTownFacilityRepositoryImpl(conn *gorm.DB) repository.TownFacilityRepository {
	return &townFacilityRepositoryImpl{Conn: conn}
}

func (rp *townFacilityRepositoryImpl) FindTownFacility(facilityId uint32) (*model_town_facility.TownFacility, error) {
	var data *model_town_facility.TownFacility
	result := rp.Conn.Where("facility_id = ?", facilityId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
