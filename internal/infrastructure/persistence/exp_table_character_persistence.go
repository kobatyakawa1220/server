package persistence

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type expTableCharacterRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableCharacterRepositoryImpl(conn *gorm.DB) repository.ExpTableCharacterRepository {
	return &expTableCharacterRepositoryImpl{Conn: conn}
}

func (rp *expTableCharacterRepositoryImpl) FindExpTableCharacters(query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) ([]*model_exp_table.ExpTableCharacter, error) {
	var datas []*model_exp_table.ExpTableCharacter
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableCharacterRepositoryImpl) FindExpTableCharacter(query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) (*model_exp_table.ExpTableCharacter, error) {
	var data *model_exp_table.ExpTableCharacter
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *expTableCharacterRepositoryImpl) GetRequiredCoinsForUpgrade(currentLevel uint8) (uint16, error) {
	var data *model_exp_table.ExpTableCharacter
	if result := rp.Conn.Where("level = ?", currentLevel).First(&data); result.Error != nil {
		return 0, result.Error
	}
	return data.RequiredCoinPerItem, nil
}
