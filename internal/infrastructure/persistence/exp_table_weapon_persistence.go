package persistence

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type expTableWeaponRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableWeaponRepositoryImpl(conn *gorm.DB) repository.ExpTableWeaponRepository {
	return &expTableWeaponRepositoryImpl{Conn: conn}
}

func (rp *expTableWeaponRepositoryImpl) FindExpTableWeapons(query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) ([]*model_exp_table.ExpTableWeapon, error) {
	var datas []*model_exp_table.ExpTableWeapon
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableWeaponRepositoryImpl) FindExpTableWeapon(query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) (*model_exp_table.ExpTableWeapon, error) {
	var data *model_exp_table.ExpTableWeapon
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *expTableWeaponRepositoryImpl) GetRequiredCoinsForUpgrade(currentLevel uint8) (uint16, error) {
	var data *model_exp_table.ExpTableWeapon
	if result := rp.Conn.Where("level = ?", currentLevel).First(&data); result.Error != nil {
		return 0, result.Error
	}
	return data.RequiredCoinPerItem, nil
}
