package infrastructure

import (
	"github.com/gorilla/mux"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/middleware"
	"gitlab.com/kirafan/sparkle/server/internal/interfaces"
)

// NewRouter creates a new router for any number of api routers
func NewRouter(middlewares middleware.Middlewares,
	router0 *interfaces.AppVersionApiController,
	router1 *interfaces.EventBannerGetAllApiController,
	router2 *interfaces.InformationGetAllApiController,
	router3 *interfaces.PlayerAbilityApiController,
	router4 *interfaces.PlayerAchievementApiController,
	router5 *interfaces.PlayerAdvApiController,
	router6 *interfaces.PlayerAgeApiController,
	router7 *interfaces.PlayerBadgeApiController,
	router8 *interfaces.PlayerBattlePartyApiController,
	router9 *interfaces.PlayerCharacterApiController,
	router10 *interfaces.PlayerChestApiController,
	router11 *interfaces.PlayerContentRoomApiController,
	router12 *interfaces.PlayerExchangeShopApiController,
	router13 *interfaces.PlayerFavoriteMemberApiController,
	router14 *interfaces.PlayerFieldPartyApiController,
	router15 *interfaces.PlayerFriendApiController,
	router16 *interfaces.PlayerGachaApiController,
	router17 *interfaces.PlayerGeneralFlagSaveApiController,
	router18 *interfaces.PlayerGetAllApiController,
	router19 *interfaces.PlayerItemApiController,
	router20 *interfaces.PlayerLoginApiController,
	router21 *interfaces.PlayerLoginBonusApiController,
	router22 *interfaces.PlayerMasterOrbApiController,
	router23 *interfaces.PlayerMissionApiController,
	router24 *interfaces.PlayerMoveApiController,
	router25 *interfaces.PlayerOfferApiController,
	router26 *interfaces.PlayerPresentApiController,
	router27 *interfaces.PlayerPushNotificationApiController,
	router28 *interfaces.PlayerPushTokenApiController,
	router29 *interfaces.PlayerQuestApiController,
	router30 *interfaces.PlayerQuestLogApiController,
	router31 *interfaces.PlayerResetApiController,
	router32 *interfaces.PlayerResumeApiController,
	router33 *interfaces.PlayerRoomApiController,
	router34 *interfaces.PlayerRoomObjectApiController,
	router35 *interfaces.PlayerScheduleApiController,
	router36 *interfaces.PlayerSetApiController,
	router37 *interfaces.PlayerSignupApiController,
	router38 *interfaces.PlayerStaminaApiController,
	router39 *interfaces.PlayerStoreApiController,
	router40 *interfaces.PlayerTownApiController,
	router41 *interfaces.PlayerTownFacilityApiController,
	router42 *interfaces.PlayerTrainingApiController,
	router43 *interfaces.PlayerTutorialApiController,
	router44 *interfaces.PlayerWeaponApiController,
	router45 *interfaces.QuestChapterGetAllApiController,
	router46 *interfaces.RoomObjectGetAllApiController,
	router47 *interfaces.PlayerImportApiController,
) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	routers := []interfaces.Router{
		router0,
		router1,
		router2,
		router3,
		router4,
		router5,
		router6,
		router7,
		router8,
		router9,
		router10,
		router11,
		router12,
		router13,
		router14,
		router15,
		router16,
		router17,
		router18,
		router19,
		router20,
		router21,
		router22,
		router23,
		router24,
		router25,
		router26,
		router27,
		router28,
		router29,
		router30,
		router31,
		router32,
		router33,
		router34,
		router35,
		router36,
		router37,
		router38,
		router39,
		router40,
		router41,
		router42,
		router43,
		router44,
		router45,
		router46,
		router47,
	}
	for _, api := range routers {
		for _, route := range api.Routes() {
			handler := route.HandlerFunc
			router.
				Methods(route.Method, "OPTIONS").
				Path(route.Pattern).
				Name(route.Name).
				Handler(middlewares.Then(handler))
		}
	}

	return router
}
