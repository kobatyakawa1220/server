package wires_providers

import (
	"github.com/google/wire"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

var UserUsecaseSet = wire.NewSet(
	persistence.NewUserRepositoryImpl,
	usecase.NewUserUsecase,
)

var VersionUsecaseSet = wire.NewSet(
	persistence.NewVersionRepositoryImpl,
	usecase.NewVersionUsecase,
)

var ChapterUsecaseSet = wire.NewSet(
	persistence.NewChapterRepositoryImpl,
	usecase.NewChapterUsecase,
)

var QuestUsecaseSet = wire.NewSet(
	persistence.NewQuestRepositoryImpl,
	usecase.NewQuestUsecase,
)

var MissionUsecaseSet = wire.NewSet(
	persistence.NewMissionRepositoryImpl,
	usecase.NewMissionUsecase,
)

var PresentUsecaseSet = wire.NewSet(
	persistence.NewPresentRepositoryImpl,
	usecase.NewPresentUsecase,
)

var LoginBonusUsecaseSet = wire.NewSet(
	persistence.NewLoginBonusRepositoryImpl,
	usecase.NewLoginBonusUsecase,
)

var NamedTypeUsecaseSet = wire.NewSet(
	persistence.NewNamedTypeRepositoryImpl,
	usecase.NewNamedTypeUsecase,
)

var CharacterUsecaseSet = wire.NewSet(
	persistence.NewCharacterRepositoryImpl,
	usecase.NewCharacterUsecase,
)

var ItemUsecaseSet = wire.NewSet(
	persistence.NewItemRepositoryImpl,
	usecase.NewItemUsecase,
)

var GachaUsecaseSet = wire.NewSet(
	persistence.NewGachaRepositoryImpl,
	usecase.NewGachaUsecase,
)

var ScheduleUsecaseSet = wire.NewSet(
	persistence.NewScheduleRepositoryImpl,
	usecase.NewScheduleUsecase,
)

var QuestWaveUsecaseSet = wire.NewSet(
	persistence.NewQuestWaveRepositoryImpl,
	usecase.NewQuestWaveUsecase,
)

var ExpTableRankUsecaseSet = wire.NewSet(
	persistence.NewExpTableRankRepositoryImpl,
	usecase.NewExpTableRankUsecase,
)

var ExpTableCharacterUsecaseSet = wire.NewSet(
	persistence.NewExpTableCharacterRepositoryImpl,
	usecase.NewExpTableCharacterUsecase,
)

var ExpTableFriendshipUsecaseSet = wire.NewSet(
	persistence.NewExpTableFriendshipRepositoryImpl,
	usecase.NewExpTableFriendshipUsecase,
)

var ExpTableSkillUsecaseSet = wire.NewSet(
	persistence.NewExpTableSkillRepositoryImpl,
	usecase.NewExpTableSkillUsecase,
)

var ExpTableWeaponUsecaseSet = wire.NewSet(
	persistence.NewExpTableWeaponRepositoryImpl,
	usecase.NewExpTableWeaponUsecase,
)

var EvoTableLimitBreakUsecaseSet = wire.NewSet(
	persistence.NewEvoTableLimitBreakRepositoryImpl,
	usecase.NewEvoTableLimitBreakUsecase,
)

var EvoTableEvolutionUsecaseSet = wire.NewSet(
	persistence.NewEvoTableEvolutionRepositoryImpl,
	usecase.NewEvoTableEvolutionUsecase,
)

var EvoTableWeaponUsecaseSet = wire.NewSet(
	persistence.NewEvoTableWeaponRepositoryImpl,
	usecase.NewEvoTableWeaponUsecase,
)

var RoomObjectUsecaseSet = wire.NewSet(
	persistence.NewRoomObjectRepositoryImpl,
	usecase.NewRoomObjectUsecase,
)

var TownFacilityUsecaseSet = wire.NewSet(
	persistence.NewTownFacilityRepositoryImpl,
	usecase.NewTownFacilityUsecase,
)

var LevelTableTownFacilityUsecaseSet = wire.NewSet(
	persistence.NewLevelTableTownFacilityRepositoryImpl,
	usecase.NewLevelTableTownFacilityUsecase,
)

var ItemTableTownFacilityUsecaseSet = wire.NewSet(
	persistence.NewItemTableTownFacilityRepositoryImpl,
	usecase.NewItemTableTownFacilityUsecase,
)

var WeaponCharacterTableUsecaseSet = wire.NewSet(
	persistence.NewWeaponCharacterTableRepositoryImpl,
	usecase.NewWeaponCharacterTableUsecase,
)

var WeaponTableUsecaseSet = wire.NewSet(
	persistence.NewWeaponRepositoryImpl,
	usecase.NewWeaponUsecase,
)

var WeaponRecipeUsecaseSet = wire.NewSet(
	persistence.NewWeaponRecipeRepositoryImpl,
	usecase.NewWeaponRecipeUsecase,
)
