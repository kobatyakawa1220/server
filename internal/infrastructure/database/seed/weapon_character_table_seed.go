package seed

import (
	"encoding/json"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedWeaponCharacterTables(db *gorm.DB) {
	tablesFile, err := Read("weapon_character_tables")
	if err != nil {
		return
	}
	var tables []model_weapon.WeaponCharacterTable
	err = json.Unmarshal(tablesFile, &tables)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(tables, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
