package seed

import (
	"encoding/json"

	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedNamedTypes(db *gorm.DB) {
	namedTypesFile, err := Read("named_types")
	if err != nil {
		return
	}
	var namedTypes []model_named_type.NamedType
	err = json.Unmarshal(namedTypesFile, &namedTypes)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(namedTypes, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
