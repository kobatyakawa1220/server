package seed

import (
	"encoding/json"

	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedTownFacilities(db *gorm.DB) {
	townFacilitiesFile, err := Read("town_facilities")
	if err != nil {
		return
	}
	var townFacilities []model_town_facility.TownFacility
	err = json.Unmarshal(townFacilitiesFile, &townFacilities)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(townFacilities, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
