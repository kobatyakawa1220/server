package seed

import (
	"encoding/json"

	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedItems(db *gorm.DB) {
	itemsFile, err := Read("items")
	if err != nil {
		return
	}
	var items []model_item.Item
	err = json.Unmarshal(itemsFile, &items)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(items, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
