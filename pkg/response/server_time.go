package response

import "time"

func ToSparkleTime(t time.Time) string {
	// Since schedule and some functions depends on JST, convert to JST
	JST := time.FixedZone("Asia/Tokyo", 9*60*60)
	tJST := t.In(JST)
	// Format the time in 2023-01-20T00:49:57Z07:00+09:00 format
	tm := tJST.Format(time.RFC3339)
	return tm
}

func NewSparkleTime() string {
	return ToSparkleTime(time.Now())
}
