package response

import "testing"

func TestNewSparkleTime(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "NewSparkleTime works",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewSparkleTime()
			t.Logf("NewSparkleTime() = %v", got)
			if len(got) != len("2023-05-15T05:52:23+09:00") {
				t.Errorf("len(NewSparkleTime()) = %v, want %v", len(got), len("2023-05-15T05:52:23+09:00"))
			}
		})
	}
}
