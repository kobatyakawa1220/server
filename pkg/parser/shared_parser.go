package parser

import (
	"strconv"
	"strings"
)

func ParseCommaStringAsInt64Array(requestString string) []int64 {
	// Split string by comma
	split := strings.Split(requestString, ",")
	// Parse to int64 array
	result := make([]int64, len(split))
	for i, s := range split {
		result[i], _ = strconv.ParseInt(s, 10, 64)
	}
	return result
}
