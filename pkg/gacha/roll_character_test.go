package gacha_test

import (
	"math"
	"math/rand"
	"testing"

	"gitlab.com/kirafan/sparkle/server/pkg/gacha"
)

func TestGachaCharacterHandlerProbability(t *testing.T) {
	source := rand.NewSource(123)
	handler := gacha.NewGachaCharacterHandler(&source)

	rarity := gacha.GachaCharacterRarity{gacha.Rarity5}
	drops := gacha.GachaCharacterDrops{
		gacha.Rarity5: []uint32{1},
		gacha.Rarity4: []uint32{},
		gacha.Rarity3: []uint32{},
	}
	pickUps := gacha.GachaCharacterDrops{
		gacha.Rarity5: []uint32{2},
		gacha.Rarity4: []uint32{},
		gacha.Rarity3: []uint32{},
	}

	// ガチャを5000回実行し、ピックアップ排出確率が意図したものに近似しているかをテスト
	const times = 5000
	const delta = 0.1 // 許容誤差
	var countNormal, countPickup int
	for i := 0; i < times; i++ {
		result, err := handler.Roll(rarity, drops, pickUps)
		if err != nil {
			t.Errorf("error: %v", err)
			return
		}
		if len(result) != 1 {
			t.Errorf("invalid result: %v", result)
			return
		}
		switch result[0] {
		case 1:
			countNormal++
		case 2:
			countPickup++
		default:
			t.Errorf("invalid result: %d", result)
			return
		}
	}
	wantPercent := 0.5
	probPickup := float64(countPickup) / float64(times)
	if math.Abs(probPickup-wantPercent) > wantPercent+delta {
		t.Errorf("pickup probability = %f, want %f", probPickup, wantPercent)
	}
	t.Logf("pickup probability = %f", probPickup)
}

func contains(s []uint32, e uint32) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}

func TestGachaCharacterHandlerBase(t *testing.T) {
	// ランダムシードを指定
	source := rand.NewSource(123)
	handler := gacha.NewGachaCharacterHandler(&source)

	rarity := gacha.GachaCharacterRarity{
		4, 3, 3, 3, 3, 3, 3, 4, 3, 4,
	}
	drops := gacha.GachaCharacterDrops{
		gacha.Rarity5: []uint32{50, 500, 5000},
		gacha.Rarity4: []uint32{40, 400, 4000},
		gacha.Rarity3: []uint32{30, 300, 3000},
	}
	pickUps := gacha.GachaCharacterDrops{
		gacha.Rarity5: []uint32{55, 555, 5555},
		gacha.Rarity4: []uint32{44, 444, 4444},
		gacha.Rarity3: []uint32{33, 333, 3333},
	}

	// test normal input
	results, err := handler.Roll(rarity, drops, pickUps)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	if len(results) != len(rarity) {
		t.Errorf("unexpected result length: %d", len(results))
	}
	for i, r := range rarity {
		if !contains(drops[r], results[i]) && !contains(pickUps[r], results[i]) {
			t.Errorf("unexpected result: %d", results[i])
		}
	}

	// test nil rarity
	_, err = handler.Roll(nil, drops, pickUps)
	if err == nil {
		t.Errorf("expected error, but got nil")
	}

	// test invalid rarity
	_, err = handler.Roll(nil, drops, pickUps)
	if err == nil {
		t.Errorf("expected error, but got nil")
	}

	// test nil drops
	_, err = handler.Roll(rarity, nil, pickUps)
	if err == nil {
		t.Errorf("expected error, but got nil")
	}

	// test invalid drops
	invalidDrops := gacha.GachaCharacterDrops{
		gacha.Rarity5: []uint32{1, 2, 3},
		gacha.Rarity4: []uint32{4, 5},
	}
	_, err = handler.Roll(rarity, invalidDrops, pickUps)
	if err == nil {
		t.Errorf("expected error, but got nil")
	}

	// test invalid pickUps
	invalidPickUps := gacha.GachaCharacterDrops{
		gacha.Rarity5: []uint32{1, 2, 3},
		gacha.Rarity4: []uint32{4, 5},
	}
	_, err = handler.Roll(rarity, drops, invalidPickUps)
	if err == nil {
		t.Errorf("expected error, but got nil")
	}
}
