package gacha

import (
	"math/rand"
	"reflect"
	"testing"
)

func TestGachaHandler_Roll(t *testing.T) {
	type args struct {
		times      int
		isChanceUp bool
		drops      GachaCharacterDrops
		pickUps    GachaCharacterDrops
	}
	tests := []struct {
		name    string
		args    args
		want    []uint32
		wantErr bool
	}{
		{
			name: "roll 10 without chanceUp",
			args: args{
				times:      10,
				isChanceUp: false,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: nil,
			},
			want: []uint32{
				34, 34, 32, 32, 43,
				41, 33, 53, 30, 31,
			},
			wantErr: false,
		},
		{
			name: "roll 10 with chanceUp",
			args: args{
				times:      10,
				isChanceUp: true,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: nil,
			},
			want: []uint32{
				40, 31, 44, 32, 33,
				31, 33, 33, 32, 30,
			},
			wantErr: false,
		},
		{
			name: "roll 1 without chanceUp",
			args: args{
				times:      1,
				isChanceUp: false,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: nil,
			},
			want: []uint32{
				32,
			},
			wantErr: false,
		},
		{
			name: "roll 1 with chanceUp",
			args: args{
				times:      1,
				isChanceUp: true,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: nil,
			},
			want: []uint32{
				42,
			},
			wantErr: false,
		},
		{
			name: "roll 10 with pickUp",
			args: args{
				times:      10,
				isChanceUp: false,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: map[Rarity][]uint32{
					Rarity5: {500},
					Rarity4: {400},
					Rarity3: {300},
				},
			},
			want: []uint32{
				44, 30, 31, 300, 31,
				32, 300, 33, 32, 300,
			},
			wantErr: false,
		},
		{
			name: "roll 1 with pickUp",
			args: args{
				times:      1,
				isChanceUp: false,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: map[Rarity][]uint32{
					Rarity5: {500},
					Rarity4: {400},
					Rarity3: {300},
				},
			},
			want: []uint32{
				300,
			},
			wantErr: false,
		},
		{
			name: "roll 10 with pickUp and chanceUp",
			args: args{
				times:      10,
				isChanceUp: true,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: map[Rarity][]uint32{
					Rarity5: {500},
					Rarity4: {400},
					Rarity3: {300},
				},
			},
			want: []uint32{
				300, 31, 300, 40, 400,
				400, 400, 300, 40, 32,
			},
			wantErr: false,
		},
		{
			name: "roll 1 with pickUp and chanceUp",
			args: args{
				times:      1,
				isChanceUp: true,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: map[Rarity][]uint32{
					Rarity5: {500},
					Rarity4: {400},
					Rarity3: {300},
				},
			},
			want: []uint32{
				300,
			},
			wantErr: false,
		},
		{
			name: "roll 3",
			args: args{
				times:      3,
				isChanceUp: false,
				drops: map[Rarity][]uint32{
					Rarity5: {50, 51, 52, 53, 54},
					Rarity4: {40, 41, 42, 43, 44},
					Rarity3: {30, 31, 32, 33, 34},
				},
				pickUps: nil,
			},
			want: []uint32{
				34, 51, 31,
			},
			wantErr: false,
		},
	}
	source := rand.NewSource(123)
	gachaHandler := NewGachaHandler(&source)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gachaHandler.Roll(tt.args.times, tt.args.isChanceUp, tt.args.drops, tt.args.pickUps)
			if (err != nil) != tt.wantErr {
				t.Errorf("GachaHandler.Roll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GachaHandler.Roll() = %v, want %v", got, tt.want)
			}
		})
	}
}
